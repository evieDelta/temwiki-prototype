package temwiki

import (
	"codeberg.org/eviedelta/temwiki/sid"
)

// Article contains the full set of information about an article including information derived from outside the file such as the name, category, path, and meta gathered by the git
type Article struct {
	ArticleConfig

	// this is defined from the filename
	Name string

	// this is generated from the file path
	// it is only sent as a string array as any tags connected to the categories will be appended to the tags list
	Category []string

	// this is generated from the file path and name, effectively its a combination of all categories and the name delimited with .
	Path ArticlePath

	// This is the hash generated from a file, it is used to tell if there is anything different with a file when importing to the database
	FileHash string

	// Metadata collected from the git repo
	GitMeta
}

// ArticleConfig is the article data that is stored on disk in file, contains the majority of the important data
type ArticleConfig struct {
	Aliases []string

	Tags []sid.ID

	Overview Page
	Pages    []Page

	Properties ViewProperties
}

// Page is a single page of an article
type Page struct {
	Description string
	Fields      []Field

	Thumbnail string
}

// Field is a field within a page
type Field struct {
	Name   string
	Value  string
	Inline bool
}
