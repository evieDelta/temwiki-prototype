package temwiki

import "strings"

// ArticlePath is a type that describes a path to an article
type ArticlePath []string

func (a ArticlePath) String() string {
	return strings.Join([]string(a), ".")
}

// Path returns a as converted into a filesystem path, basically replaces . with /
func (a ArticlePath) Path() string {
	return strings.Join([]string(a), "/")
}

// Split returns the last element of the path as the name, and the rest as the categories
func (a ArticlePath) Split() ([]string, string) {
	i := len(a)
	switch i {
	case 0:
		return []string{}, ""
	case 1:
		return []string{}, a[0]
	default:
		return a[:i-1], a[i-1]
	}
}

// Name is like split but only gives the name
func (a ArticlePath) Name() string {
	_, n := a.Split()
	return n
}

// Categories is like split but only gives the categories
func (a ArticlePath) Categories() []string {
	c, _ := a.Split()
	return c
}

// GenArticlePath turns a string array into an article path, the last element of s will be the name, and anything prior will be the categories
func GenArticlePath(s []string) (p ArticlePath) {
	art := ArticlePath(s)
	for i, x := range art {
		art[i] = NameCleaner(x)
	}
	return art
}

// GenArticlePathExt is functionally identical to GenArticlePath just with a different calling footprint
func GenArticlePathExt(s ...string) (p ArticlePath) {
	return GenArticlePath(s)
}

// ToArticlePath converts an article name + a list of categories into an article path
func ToArticlePath(name string, cat []string) (p ArticlePath) {
	art := ArticlePath(append(cat, name))
	for i, x := range art {
		art[i] = NameCleaner(x)
	}
	return art
}

// ToArticlePathExt is functionally identical to ToArticlePath just with a different footprint
func ToArticlePathExt(name string, cat ...string) ArticlePath {
	return ToArticlePath(name, cat)
}

// ParseArticlePath takes a string path (eg colors/green/stuff) and turns it into an article, using either ` `, `.`, or `/` as a seperator,
func ParseArticlePath(path string) ArticlePath {
	path = NameCleaner(path)

	// get a total of `.`, `/`, and ` `
	i := strings.Count(path, ".")
	i += strings.Count(path, "/")
	i += strings.Count(path, " ")

	// make a list with a capacity of that
	p := make(ArticlePath, 0, i)

	buf := strings.Builder{}
	for _, x := range path {
		switch x {
		case ' ', '.', '/':
			if buf.Len() == 0 {
				continue
			}
			p = append(p, buf.String())
			buf.Reset()
		default:
			buf.WriteRune(x)
		}
	}

	p = append(p, buf.String())

	return p
}
