package temwiki

import "codeberg.org/eviedelta/temwiki/sid"

// Category defines the whole category object including data not included in file
type Category struct {
	Name ArticlePath

	CategoryConfig
}

// CategoryConfig defines the data for a category placed in !category.yaml
type CategoryConfig struct {
	// a list of tags to corrilate to
	Tags []sid.ID

	Description string
}
