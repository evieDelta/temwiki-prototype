package main

import (
	"flag"
	"fmt"

	"codeberg.org/eviedelta/temwiki/cmd/internal/config"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temapi"
	"codeberg.org/eviedelta/temwiki/temcore"
	"codeberg.org/eviedelta/temwiki/temdb/tpsql"
	"codeberg.org/eviedelta/temwiki/temfs"
)

type configuration struct {
	Host struct {
		Hostname string
		Port     string
	}
	Wiki struct {
		Directory string
	}
	Database struct {
		Host     string
		User     string
		Pass     string
		Database string
	}
}

func main() {
	var configDir string
	flag.StringVar(&configDir, "config", "./", "the config directory")
	flag.Parse()

	cfg := new(configuration)

	err := config.Load(configDir, "config", cfg)
	if err != nil {
		panic(err)
	}

	dtrace.Printfln("%+v", cfg)

	dsn := fmt.Sprintf("dbname=%v user=%v password='%v' host='%v' sslmode=disable",
		cfg.Database.Database, cfg.Database.User, cfg.Database.Pass, cfg.Database.Host)

	db := tpsql.New(dsn)

	fs, err := temfs.New(cfg.Wiki.Directory, false, &temfs.Options{})
	if err != nil {
		panic(err)
	}

	if cfg.Host.Port == "" {
		cfg.Host.Port = "80"
	}

	wiki := &temcore.Wiki{
		DB: db,
		FS: fs,
	}

	api := temapi.New(cfg.Host.Hostname+":"+cfg.Host.Port, wiki)

	err = api.Run()
	if err != nil {
		fmt.Println(err)
	}
}

// congradulations you have found the pineapple
