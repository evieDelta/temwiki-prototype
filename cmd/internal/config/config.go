package config

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"reflect"
	"strings"

	"github.com/goccy/go-yaml"
	"github.com/pelletier/go-toml"
)

// Locations contains default locations that'll be checked
var Locations = []string{
	".",
	"./data",
	"./data/config",
	"./config",
}

var Extensions = []string{
	"",
	".config",
	".conf",
	".cfg",
	".toml",
	".ini",
	".json",
	".yaml",
	".yml",
	".xml",
}

// CheckExist is a lazy function to determine if a file exists or not
func CheckExist(loc string) bool {
	i, err := os.Stat(loc)
	if err != nil || i.IsDir() {
		return false
	}
	return true
}

func FindWithAnyExtension(dir, file string, extensions []string) (string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return "", err
	}
	for _, x := range files {
		// fmt.Println(x.Name(), x.Size(), x.Mode(), x.IsDir())
		if x.IsDir() {
			continue
		}
		ext := path.Ext(x.Name())
		if extensions != nil {
			// fmt.Println("checking extensions, ", ext)
			for _, y := range extensions {
				// fmt.Println(y)
				if ext == y {
					// fmt.Println("was ", y)
					goto passextensions
				}
			}
			// fmt.Println("no proper extension")
			continue
		}
		// fmt.Println("no extension requirements")
	passextensions:
		p := strings.TrimSuffix(x.Name(), ext)
		// fmt.Println(p)

		if p == file {
			// fmt.Println("found,", x.Name())
			return x.Name(), nil
		}
	}
	return "", ErrorNotFound
}

// CheckDirExist is a lazy function to determine if a directory exists or not
func CheckDirExist(loc string) bool {
	i, err := os.Stat(loc)
	if err != nil || !i.IsDir() {
		return false
	}
	return true
}

// Load will attempt to unmarshal the config file in the location defined by prefix into the struct given
func Load(loc, filename string, cfg interface{}) error {
	if reflect.TypeOf(cfg).Kind() != reflect.Ptr {
		return ErrorEnvNotPointer
	}

	var search []string
	if loc == "" {
		search = Locations
	} else {
		search = append([]string{loc}, Locations...)
	}

	var data []byte

	for _, f := range search {
		if !CheckDirExist(f) {
			continue
		}
		file := path.Join(f, filename)

		if path.Ext(file) != "" {
			if !CheckExist(file) {
				continue
			}
		} else {
			fname, err := FindWithAnyExtension(f, filename, Extensions)
			if err != nil {
				if os.IsNotExist(err) || err == ErrorNotFound {
					continue
				}
				return err
			}
			file = path.Join(f, fname)
		}

		var err error
		data, err = ioutil.ReadFile(file)
		if err != nil {
			return err
		}

		switch path.Ext(file) {
		default:
			fallthrough
		// in most cases toml is the default for this, and ini is close enough to fit in the toml standard
		case "", ".toml", ".ini", ".config", ".conf", ".cfg":
			err = toml.Unmarshal(data, cfg)

		case ".json":
			err = json.Unmarshal(data, cfg)

		case ".yaml", ".yml":
			err = yaml.Unmarshal(data, cfg)

		// i have no idea why one would ever want to use this but i guess its here if one so desires to use xml for some reason
		case ".xml":
			err = xml.Unmarshal(data, cfg)
		}

		if err != nil {
			return err
		}

		err = EnvvarUnmarshal(strings.TrimSuffix(filename, path.Ext(filename)), cfg)
		if err != nil {
			return err
		}

		return nil
	}

	return fmt.Errorf("Filename not found, locations checked: %v", search)
}
