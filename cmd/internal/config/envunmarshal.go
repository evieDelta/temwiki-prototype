package config

import (
	"encoding/json"
	"os"
	"reflect"
	"strings"
)

type Unmarshaler interface {
	UnmarshalEnv(string) error
}

func EnvvarUnmarshal(prefix string, val interface{}) error {
	v := reflect.ValueOf(val)
	if v.Kind() != reflect.Ptr {
		return ErrorEnvNotPointer
	}

	e := v.Elem()

	if e.Kind() != reflect.Struct {
		return ErrorEnvNotStruct
	}

	for i := 0; i < e.NumField(); i++ {
		f := e.Type().Field(i)
		x := e.Field(i)

		fname := f.Tag.Get("envvar")
		if fname == "" {
			continue
			// fname = f.Name
		}
		if prefix != "" {
			fname = strings.Title(fname)
		}

		if isStruct(x) {
			err := EnvvarUnmarshal(prefix+fname, x.Interface())
			if err != nil {
				return err
			}
		} else {
			data, ok := os.LookupEnv(prefix + fname)
			if !ok || !x.CanAddr() {
				continue
			}

			calvar := x
			if calvar.Kind() != reflect.Ptr {
				calvar = x.Addr()
			}

			if unm, ok := calvar.Interface().(Unmarshaler); ok {
				err := unm.UnmarshalEnv(data)
				if err != nil {
					return err
				}

			} else {
				switch x.Kind() {
				case reflect.String:
					x.SetString(data)

				default:
					err := json.Unmarshal([]byte(data), calvar.Interface())
					if err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

func isStruct(val interface{}) bool {
	if v, ok := val.(reflect.Value); ok {
		if v.Kind() == reflect.Struct {
			return true
		}
		if v.Kind() == reflect.Ptr {
			e := v.Elem()
			if e.Kind() == reflect.Struct {
				return true
			}
		}
		return false
	}
	if t, ok := val.(reflect.Type); ok {
		if t.Kind() == reflect.Struct {
			return true
		}
		if t.Kind() == reflect.Ptr {
			e := t.Elem()
			if e.Kind() == reflect.Struct {
				return true
			}
		}
		return false
	}
	v := reflect.ValueOf(val)
	if v.Kind() == reflect.Struct {
		return true
	}
	if v.Kind() == reflect.Ptr {
		e := v.Elem()
		if e.Kind() == reflect.Struct {
			return true
		}
	}
	return false
}

type envError string

func (e envError) Error() string {
	return string(e)
}

// Errors that may be given by EnvvarUnmarshal
const (
	ErrorEnvNotStruct  envError = "Given value is not a struct type"
	ErrorEnvNotPointer envError = "Given value is not a pointer"
	ErrorNotFound      envError = "No file found"
)
