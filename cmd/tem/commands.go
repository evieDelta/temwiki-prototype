package main

import (
	"bytes"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/tabwriter"

	"codeberg.org/eviedelta/temwiki/temfs"
)

var gcmds = &command{
	Help: globalHelp,

	SubCommands: map[string]*command{
		"tag":  tag,
		"repo": cRepo,
	},
}

type command struct {
	Description string
	Help        string
	Args        string

	SubCommands map[string]*command

	RequireRepo bool

	Exec func(*temfs.Filesystem, []string) error
}

func (c *command) commandRouter(fs *temfs.Filesystem, args []string) (bool, error) {
	cmds := c.SubCommands

	if len(args) == 0 {
		return false, nil
	}
	if args[0] == "help" {
		fmt.Println(c.generateHelp())
		return true, nil
	}
	if cmds == nil {
		return false, nil
	}
	if cmds[args[0]] == nil {
		return false, nil
	}
	ok, err := cmds[args[0]].commandRouter(fs, args[1:])
	if err != nil {
		return true, err
	}
	if ok {
		return ok, nil
	}
	if cmds[args[0]].RequireRepo && fs == nil {
		return true, errNoRepo
	}
	argCheck(args[1:], cmds[args[0]].Args)
	return true, cmds[args[0]].Exec(fs, args[1:])
}

func (c *command) generateHelp() string {
	cmds := c.SubCommands

	var list = make(
		[]struct {
			*command
			name string
		}, 0, len(cmds))

	for i, x := range cmds {
		list = append(list, struct {
			*command
			name string
		}{x, i})
	}

	sort.Slice(list, func(a, b int) bool { return list[a].name > list[b].name })

	var s = bytes.NewBuffer([]byte{})
	var t = tabwriter.NewWriter(s, 0, 4, 0, ' ', tabwriter.AlignRight)

	if c.Description != "" {
		fmt.Fprintln(t, " |", strings.Join(SplitLength(c.Description, 70, " "), "\n | "))
	}
	if c.Args != "" {
		fmt.Fprintln(t, "> Arguments:", c.Args)
	}
	if c.Help != "" {
		fmt.Fprintln(t, strings.Join(SplitLength(c.Help, 80, " "), "\n"))
	}
	fmt.Fprintln(t)

	for _, x := range list {
		desc := strings.Join(SplitLength(x.Description, 60, " "), "\n")
		desc = strings.ReplaceAll(desc, "\n", "\n\v\v╙ ")
		desc = strings.Replace(desc, "╙", "╟", strings.Count(desc, "╙")-1)
		rc := strings.Count(desc, "\n")
		dsym := ""
		if rc > 0 {
			dsym = "╥"
		} else if len(desc) > 1 {
			dsym = "─"
		}
		fmt.Fprintln(t, " -\t", x.name, "\t"+dsym, desc)
	}
	err := t.Flush()
	if err != nil {
		panic(err) // unlikely to ever happen so just panic
	}

	return s.String()
}

func argCheck(s []string, schema string) {
	if len(s) < strings.Count(schema, "<") {
		fmt.Println("not enough arguments for this command")
		fmt.Println("expected", schema)
		os.Exit(1)
	}
}
