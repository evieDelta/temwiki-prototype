package main

import (
	"io/ioutil"
	"os"
	"strings"

	"codeberg.org/eviedelta/temwiki/temerror"
	"codeberg.org/eviedelta/temwiki/temfs"
)

// findRepo attempts to find a repo in the current or parent directories
func findRepo() (dr string, err error) {
	dr, err = os.Getwd()
	var fls []os.FileInfo
	for /* strings.Count(dr, string(os.PathSeparator)) > 0 */ {
		fls, err = ioutil.ReadDir(dr)
		if err != nil {
			return dr, err
		}
		var hasCfg, hasGit bool
		for _, x := range fls {
			if strings.HasPrefix(x.Name(), temfs.ConfigRepository+".") {
				hasCfg = true
			}
			if x.IsDir() && x.Name() == ".git" {
				hasGit = true
			}
			if hasCfg && hasGit {
				return dr, nil
			}
		}
		index := strings.LastIndex(dr, string(os.PathSeparator))
		if index == -1 {
			return dr, errNoRepo
		}
		dr = dr[:index]
	}
	//	return dr, errors.New("this state should never be reached")
}

const errNoRepo = temerror.Cause("could not find directory with repository. and .git file")
