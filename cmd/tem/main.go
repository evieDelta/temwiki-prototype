package main

import (
	"fmt"
	"os"

	"codeberg.org/eviedelta/temwiki/temfs"
)

const globalHelp = `tem is a utility that contains some helper functions for managing a temora wiki tree by hand

to get started you'll probably want to use repo init`

func main() {
	args := os.Args[1:]

	var hasRepo bool
	var directory string
	if len(args) >= 2 && args[0] == "-dir" {
		directory = args[1]
		args = args[2:]
		hasRepo = true
	} else {
		var err error
		directory, err = findRepo()
		if err != nil && !os.IsNotExist(err) {
			fmt.Println(err)
			os.Exit(1)
		} else if err == nil {
			hasRepo = true
		}
	}

	var fs *temfs.Filesystem

	if hasRepo {
		var err error
		fs, err = temfs.New(directory, false, &temfs.Options{
			StrictParse: true,
		})
		if err != nil {
			fmt.Println("failed to open repository")
			fmt.Println(">", err)
			os.Exit(1)
		}
	}

	if len(args) == 0 {
		fmt.Println(gcmds.generateHelp())
		os.Exit(0)
	}

	ok, err := gcmds.commandRouter(fs, args)
	if err != nil {
		if err == errNoRepo {
			fmt.Println("This command requires being in an active repo and the command could not find a repository in current or parent directory")
			fmt.Println("(use `" + os.Args[0] + " -dir <path> <command>` to specify a directory located elsewhere)")
			os.Exit(1)
		}
		fmt.Println(err)
		os.Exit(1)
	}
	if !ok {
		fmt.Println(gcmds.generateHelp())
		os.Exit(1)
	}
}
