package main

import "strings"

// SplitLength is similar to strings.SplitAfter except it only splits on sep when a string exceedes a certain max length, the splitting behaviour attemps to keep all returned strings as close to but below the max lenght.
// note that as it only splits on instances of seperator any segment larger than the max that does not contain sep will remain intact and thus will be larger than max, as such the string length is not gaurenteed to be below the max.
func SplitLength(s string, max int, sep string) []string {
	x := strings.Split(s, sep)
	return CombineUntil(x, max, sep)
}

// CombineUntil uses similar logic to SplitLenght, except that it takes an array of strings directly rather than splitting them itself
// it essentially tries to combine the input strings until its as close to max length but not over, and returns an array of strings which are combined from s to be as close to max
func CombineUntil(s []string, max int, sep string) []string {
	// the "uses similar logic to" in the comments is actually the other way around tbh, its said that way since initially this was just a copy of SplitLenght but i've sence removed the unecessary duplicate code and haven't updated the comments yet
	var out []string
	var buf string
	for _, z := range s {
		if len(buf)+len(z) >= max && buf != "" {
			out = append(out, buf)
			buf = ""
		}
		buf += z + sep
	}
	if buf != "" {
		out = append(out, buf)
	}

	return out
}
