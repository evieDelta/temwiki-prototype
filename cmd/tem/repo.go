package main

import (
	"fmt"

	"codeberg.org/eviedelta/temwiki/temfs"
	"github.com/go-git/go-git/v5"
)

var cRepo = &command{
	Description: "Some various commands for repo management",

	SubCommands: map[string]*command{
		"init":  cRepoInit,
		"clone": cRepoClone,
	},
}

func init() {
	// in a separate init function so there isn't an init loop
	cRepo.Exec = func(fs *temfs.Filesystem, s []string) error {
		fmt.Println(cRepo.generateHelp())

		return nil
	}
}

var cRepoInit = &command{
	Description: "Initialise a new repository",
	Args:        "<path> [?file-format: yaml|toml|json (default: yaml)]",

	Exec: func(fs *temfs.Filesystem, s []string) error {
		format := ""
		if len(s) > 1 {
			format = "." + s[1]
		}
		repo, err := temfs.Initialise(s[0], format)
		if err != nil {
			return err
		}
		fs, err = temfs.NewFromRepo(false, &temfs.Options{
			StrictParse: true,
		}, repo)
		if err != nil {
			return err
		}
		fmt.Println("Initialised new repository at ", s[0])
		return nil
	},
}

var cRepoClone = &command{
	Description: "Clone a repository from a remote source",
	Args:        "<url> <path>",

	Exec: func(fs *temfs.Filesystem, s []string) error {
		repo, err := git.PlainClone(s[1], false, &git.CloneOptions{
			URL: s[0],
		})
		if err != nil {
			return err
		}

		fs, err = temfs.NewFromRepo(false, &temfs.Options{
			StrictParse: true,
		}, repo)
		if err != nil {
			return err
		}
		fmt.Println("Initialised new repository at ", s[0])
		return nil
	},
}
