package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"sort"
	"strings"
	"text/tabwriter"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
	"codeberg.org/eviedelta/temwiki/temfs"
)

var tag = &command{
	Description: "Some various commands to help with managing tags",

	SubCommands: map[string]*command{
		"new":  tagNew,
		"list": tagList,
		"find": tagFind,
		"edit": tagEdit,
	},
}

func init() {
	// in a separate init function so there isn't an init loop
	tag.Exec = func(fs *temfs.Filesystem, s []string) error {
		fmt.Println(tag.generateHelp())

		return nil
	}
}

var tagNew = &command{
	Description: "Create a new tag, takes an argument for name",
	RequireRepo: true,
	Args:        "<name>",

	Exec: func(fs *temfs.Filesystem, s []string) error {
		tag := temwiki.Tag{
			TagConfig: temwiki.TagConfig{
				Name: s[0],
			},
			ID: sid.Default.Get(),
		}
		err := fs.WriteTag(nil, tag)
		if err != nil {
			return err
		}
		fmt.Println("created new tag with ID: ", tag.ID)
		return nil
	},
}

var tagList = &command{
	Description: "lists out a list of all tags in alphabetical order",
	RequireRepo: true,

	Exec: func(fs *temfs.Filesystem, s []string) error {
		tags, err := fs.GetAllTags()
		if err != nil {
			return err
		}

		var b = bytes.NewBuffer([]byte{})
		var t = tabwriter.NewWriter(b, 0, 2, 0, ' ', tabwriter.AlignRight)

		sort.Slice(tags, func(a, b int) bool {
			return tags[a].Name < tags[b].Name
		})

		for _, y := range tags {
			desc := strings.Join(SplitLength(y.Description, 60, " "), "\n")
			desc = strings.ReplaceAll(desc, "\n", "\n\v\v\v╙ ")
			desc = strings.Replace(desc, "╙", "╟", strings.Count(desc, "╙")-1)
			dsym := ""
			if rc := strings.Count(desc, "\n"); rc > 0 {
				dsym = "╥"
			} else if len(desc) > 1 {
				dsym = "─"
			}
			fmt.Fprintln(t, " ", y.Name, "\v-", y.ID, "\v"+dsym, desc)
		}
		t.Flush()

		fmt.Println(b.String())

		return nil
	},
}

var tagFind = &command{
	Description: "Attempts to find a tag by name, takes an argument for name",
	RequireRepo: true,
	Args:        "<name>",

	Exec: func(fs *temfs.Filesystem, s []string) error {
		tags, err := fs.GetAllTags()
		if err != nil {
			return err
		}

		rank := make(map[int][]struct {
			diff int
			*temwiki.Tag
		})

		name := s[0]

		for _, x := range tags {
			diff := len(name)

			ldif := len(x.Name) - len(name)
			if ldif < 0 {
				ldif = -ldif
			}

			//	if ldif != 0 {
			//		diff -= (100 - (100 / ldif))
			//	}

			checking := name
			other := x.Name
			if len(other) < len(checking) {
				checking = x.Name
				other = name
			}

			rother := []rune(other)

			var weight int
			for i, x := range checking {
				if x != rother[i+weight] {
					if weight < ldif && ((i+1 < len(checking) && i+weight+1 < len(rother)) && []rune(checking)[i] == rother[i+weight+1]) {
						weight++
					}
					diff--
				}
			}

			if diff < 1 {
				continue
			}

			rank[diff] = append(rank[diff], struct {
				diff int
				*temwiki.Tag
			}{diff, x})
		}

		var b = bytes.NewBuffer([]byte{})
		var t = tabwriter.NewWriter(b, 0, 2, 0, ' ', tabwriter.AlignRight)

		srank := make([]struct {
			diff int
			*temwiki.Tag
		}, 0, len(rank))

		for _, x := range rank {
			for _, y := range x {
				srank = append(srank, y)
			}
		}

		sort.Slice(srank, func(a, b int) bool {
			return srank[a].diff > srank[b].diff
		})

		count := 0
		for _, y := range srank {
			if count > 5 {
				break
			}
			count++
			desc := strings.Join(SplitLength(y.Description, 60, " "), "\n")
			desc = strings.ReplaceAll(desc, "\n", "\n\v\v\v╙ ")
			desc = strings.Replace(desc, "╙", "╟", strings.Count(desc, "╙")-1)
			dsym := ""
			if rc := strings.Count(desc, "\n"); rc > 0 {
				dsym = "╥"
			} else if len(desc) > 1 {
				dsym = "─"
			}
			fmt.Fprintln(t, y.diff, "\t", y.Name, "\v-", y.ID, "\v"+dsym, desc)
		}
		t.Flush()

		fmt.Println(b.String())

		return nil
	},
}

var tagEdit = &command{
	Description: "Open a tag file in a text editor (probably only works on linux as it execs to nano, and might just not work at all), takes an argument for ID",
	RequireRepo: true,
	Args:        "<id>",

	Exec: func(fs *temfs.Filesystem, s []string) error {
		id, err := sid.ParseIDString(s[0])
		if err != nil {
			return err
		}

		tag, err := fs.GetTag(id)
		if err != nil {
			return err
		}

		data, err := fs.A.Conf.Marshal(tag)
		if err != nil {
			return err
		}

		file, err := ioutil.TempFile("", s[0]+"-*"+fs.A.Conf.FileExtension())
		if err != nil {
			return err
		}
		defer func() {
			name := file.Name()
			file.Close()
			err := os.Remove(name)
			if err != nil {
				fmt.Println(err)
			}
		}()
		_, err = file.Write(data)
		if err != nil {
			return err
		}

		for {
			cmd := exec.Command("nano", file.Name())
			cmd.Stdin = os.Stdin
			cmd.Stderr = os.Stderr
			cmd.Stdout = os.Stdout

			err := cmd.Run()
			if err != nil {
				fmt.Println(err)
			}

			_, err = file.Seek(0, io.SeekStart)
			if err != nil {
				return err
			}

			dat, err := ioutil.ReadAll(file)
			if err != nil {
				return err
			}

			if string(data) != string(dat) {
				fmt.Println("Data modified")
			} else {
				fmt.Println("no modifications made")
				return nil
			}

			ntag := temwiki.Tag{}

			err = fs.A.Conf.Unmarshal(dat, &ntag)
			if err != nil {
				fmt.Println("an error has occoured")
				fmt.Println(err)
				fmt.Println("would you like to retry? true/false")
				var ok bool
				fmt.Scan(&ok)
				if ok {
					continue
				} else {
					return err
				}
			}

			return fs.WriteTag(nil, ntag)
		}
	},
}
