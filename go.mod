module codeberg.org/eviedelta/temwiki

go 1.16

require (
	github.com/ProtonMail/gopenpgp/v2 v2.1.4
	github.com/diamondburned/arikawa/v2 v2.0.0-20210106051538-d9a159d948af
	github.com/georgysavva/scany v0.2.7
	github.com/go-chi/chi v1.5.1
	github.com/go-git/go-billy/v5 v5.0.0
	github.com/go-git/go-git/v5 v5.2.0
	github.com/goccy/go-yaml v1.8.4
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgconn v1.8.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pelletier/go-toml v1.8.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
)
