package dtrace

import (
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var mu sync.Mutex

const timeformat = "2006-01-02 15:04:05"

var buffers = sync.Pool{
	New: func() interface{} {
		return new(strings.Builder)
	},
}

var debug = os.Getenv("DEBUG") != ""

func Print(s ...interface{}) {
	if debug {
		mu.Lock()
		defer mu.Unlock()
		os.Stderr.WriteString(fmt.Sprint(s...))
	}
}

func Println(s ...interface{}) {
	if debug {
		mu.Lock()
		defer mu.Unlock()
		b := buffers.Get().(*strings.Builder)
		b.WriteString(time.Now().Format("2006-01-02 15:04:05"))
		getWhere(b, 1)
		b.WriteRune('\n')
		b.WriteString(fmt.Sprint(s...))
		os.Stderr.WriteString(strings.ReplaceAll(b.String(), "\n", "\n | "))
		os.Stderr.Write(linebreak)
		b.Reset()
		buffers.Put(b)
	}
}

func Printf(f string, s ...interface{}) {
	if debug {
		mu.Lock()
		defer mu.Unlock()
		b := buffers.Get().(*strings.Builder)
		b.WriteString(fmt.Sprintf(f, s...))
		os.Stderr.WriteString(b.String())
		b.Reset()
		buffers.Put(b)
	}
}

func Printfln(f string, s ...interface{}) {
	if debug {
		mu.Lock()
		defer mu.Unlock()
		b := buffers.Get().(*strings.Builder)
		b.WriteString(time.Now().Format("2006-01-02 15:04:05"))
		getWhere(b, 1)
		b.WriteRune('\n')
		b.WriteString(fmt.Sprintf(f, s...))
		os.Stderr.WriteString(strings.ReplaceAll(b.String(), "\n", "\n | "))
		os.Stderr.Write(linebreak)
		b.Reset()
		buffers.Put(b)
	}
}

var baseDir string

func init() {
	_, f, _, ok := runtime.Caller(0)
	if ok {
		baseDir = strings.TrimSuffix(path.Dir(f), "internal/dtrace")
	}
}

var space = []byte{' '}
var colon = []byte{':'}
var linebreak = []byte{'\n'}

func getWhere(b io.Writer, skip int) {
	_, f, l, ok := runtime.Caller(1 + skip)
	if ok {
		b.Write(space)
		io.WriteString(b, strings.TrimPrefix(f, baseDir))
		b.Write(colon)
		io.WriteString(b, strconv.Itoa(l))
		b.Write(space)
	}
}
