package retc

import (
	"fmt"
	"strings"
	"unicode"
)

func FormatStruct(s interface{}) string {
	return FormatterThing(fmt.Sprintf("%+v", s))
}

// FormatterThing does some really basic formatting on a string, replacing spaces with linebreaks and adding indentation based off of {} brackets
func FormatterThing(s string) string {
	x := []rune(s)
	var out string
	var state int
	var after bool
	var quote bool
	for i, z := range x {
		if z == '"' {
			quote = !quote
			out += string(z)
		}
		if quote {
			if z == '\n' {
				out += "\n"
				out += strings.Repeat(" ", state*2)
			}
			continue
		}
		after = false
		if unicode.IsSpace(z) {
			if z == '\n' {
				out += "\n"
				out += strings.Repeat(" ", (state+1)*2)
				continue
			}
			var ns int = -1
			if len(x) > i {
				ns = strings.IndexFunc(string(x[i+1:]), unicode.IsSpace)
			}
			nc := strings.Index(string(x[i:]), ":")
			if ns == -1 || ns > nc {
				out += "\n"
				out += strings.Repeat(" ", state*2)
				continue
			}
		}
		if z == '}' {
			state--
			out += "\n" + strings.Repeat(" ", state*2)
		}
		if z == '{' {
			after = true
			state++
		}
		out += string(z)
		if after {
			out += "\n" + strings.Repeat(" ", state*2)
		}
	}
	return out
}
