package temwiki

import (
	"regexp"
	"time"
	"unicode"

	"codeberg.org/eviedelta/temwiki/temerror"
)

// GitMeta contains extra meta gathered from the git repository about when something was created and edited
type GitMeta struct {
	Modified Commit
	Created  Commit
}

// Commit contains extra meta gathered from the git repository about the author, the time it was done, the commit hash, and the signature
type Commit struct {
	When time.Time

	CommitHash string `json:"-"`

	PGPSignature string
	Verified     bool

	By CommitAuthor
}

// CommitAuthor contains the author information encoded by git
type CommitAuthor struct {
	// Name is the name given for the author
	// EmailID may be the email of the author, or it may be the UserID of the author (may be packaged into a psudoemail like 123456789-noreply@ids.somebot.lan)
	Name    string
	EmailID string
}

// Abomination regex used for seeing if there is a discord ID encoded in the email field
var emailIDRegex = regexp.MustCompile(
	`^(?:id|discord|noreply)?(?:-)?(\d)+(?:-)?(?:noreply|discord|id|)?(?:@(?:ms|discord|mail|id|discordid|user|userid|discorduser)s?(?:\.[\w\d-_]+)?\.[\w]{2,})?$`,
)

// See if the git name field contains an id in the format `username#disc (id)` *exactly* (eg: `username#0000 (123456789)`)
var usernameIDRegex = regexp.MustCompile(`^.+#\d{4}\ \(([\d]+)\)$`)

// GetID tries to see if it can find a discord userid in the commit author information
func (c CommitAuthor) GetID() (string, error) {
	if isAllNum(c.EmailID) {
		return c.EmailID, nil
	}

	ret := emailIDRegex.Find([]byte(c.EmailID))
	if ret != nil {
		return string(ret), nil
	}

	ret = usernameIDRegex.Find([]byte(c.EmailID))
	if ret != nil {
		return string(ret), nil
	}

	return "", ErrorNoIDFoundInCommitAuthor
}

func isAllNum(s string) bool {
	for _, x := range s {
		if !unicode.IsDigit(x) {
			return false
		}
	}
	return true
}

// ErrorNoIDFoundInCommitAuthor is returned by GetID if it couldn't find a userID in either Commit EmailID or Commit Name
const ErrorNoIDFoundInCommitAuthor temerror.Cause = "Could not find a discord userID"
