package temwiki

import (
	"strings"
	"unicode"
)

// some symbols to replace, don't allow ! because it is used for some internal purposes
var nameReplacer = strings.NewReplacer("!", "", "_", "-")

// NameCleaner processes a string converting it to lowercase and replacing any spaces with -'s
// to make things more consistant for identifiers
func NameCleaner(s string) string {
	s = strings.ToValidUTF8(s, "") // remove any invalid characters
	s = strings.ToLower(s)         // lowercase the text
	s = strings.Trim(s, "./ ")     // remove any leading or trailing instances of `.` `/` or ` `

	// Replace any space characters with -
	{
		r := []rune(s)
		for i, x := range r {
			if unicode.IsSpace(x) {
				r[i] = '-'
			}
		}
		s = string(r)
	}

	// Remove some unwanted characters
	s = nameReplacer.Replace(s)

	return s
}

var nameNatReplacer = strings.NewReplacer("-", " ")

// NameNaturaliser is somewhat the inverse of NameCleaner, it'll replace `-` with ` `, and optionally it'll add some capitalisation
func NameNaturaliser(s string, caps NameCapitalisation) string {
	s = nameNatReplacer.Replace(s)

	switch caps {
	default:
		fallthrough
	case NameNoCapitalisation:
		return s
	case NameStartCapitalisation:
		[]rune(s)[0] = unicode.ToUpper([]rune(s)[0])
		return s
	case NameTitleCase:
		return strings.Title(s)
	}
}

// NameCapitalisation is used for function NameNaturaliser to define how it'll capitalise the name
type NameCapitalisation uint8

// These are used for NameNaturaliser to pick how to capitalise it
const (
	NameNoCapitalisation NameCapitalisation = iota
	NameStartCapitalisation
	NameTitleCase
)
