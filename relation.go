package temwiki

import "codeberg.org/eviedelta/temwiki/sid"

type RelationType uint

// The types of relation
// Since this is also going to be saved to external files i am not using iota to ensure these don't change
const (
	// Should not be used in practise, but here just in case
	RelationTypeUnknown = 0

	// This type marks that this tag will make the thing with it also have this tag (eg, something tagged with 'blue' will also be tagged with 'colors')
	RelationTypeIsAlso = 1
)

type Relation struct {
	Type RelationType

	With sid.ID
}
