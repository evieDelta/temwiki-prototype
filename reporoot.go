package temwiki

// RootMeta contains the meta data about the repository
type RootMeta struct {
	// The name of the repository
	Name string

	// A short description of the repository
	Description string

	Version int
}

const FormatVersion = 0
