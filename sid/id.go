package sid

import (
	"math/rand"
	"time"
)

var Default = Generator{
	Epoch: time.Unix(1609529715, 0), // the time i wrote this lmao

	// Reduce the risk of conflicting IDs by starting things off at random points
	Worker:    rand.New(rand.NewSource(time.Now().UnixNano())).Intn(0b1111111111),
	increment: uint64(rand.New(rand.NewSource(time.Now().Unix())).Intn(0xFFF)),
}
