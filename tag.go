package temwiki

import "codeberg.org/eviedelta/temwiki/sid"

// TagAutocomplete contains some minimal information about a tag, useful for autocomplete suggestions
type TagAutocomplete struct {
	Name        string
	Description string
	ID          sid.ID
}

// Tag contains the full tag information including information not stored onfile such as the ID (which is normally the filename)
type Tag struct {
	TagConfig

	ID sid.ID
}

// TagConfig is the tag information stored in file, it contains most of the details and settings about a tag
type TagConfig struct {
	Name        string
	Description string
	//	Article     []string // may be implemented at a later date, for now you can just make an article of the same name somewhere

	Properties ViewProperties
	Notes      ContentNotes

	Relations []Relation
}

// ViewProperties contains some properties that effect how something may show
type ViewProperties struct {
	BlockSearch bool // Don't show up in search results unless there is a direct match
	BlockRandom bool // Don't show up via random commands
	Spoiler     bool // Hide the main content behind spoilers to require a user to manually click it to view

}

// ContentNotes contains disclaimers that may be placed on an article or tag
// Typically these should only be on articles and should only be added to tags in extreme circumstances
type ContentNotes struct {
	ContentWarning string // A default bit of text that will appear in the content warning section
	Disclaimer     string // A default bit of text that will appear in the disclaimer section

	Other map[string]string
}
