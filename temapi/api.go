package temapi

import (
	"log"
	"net/http"

	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temapi/temauth"
	"codeberg.org/eviedelta/temwiki/temcore"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func New(address string, w *temcore.Wiki) *API {
	r := chi.NewRouter()

	var a = &API{
		r:       r,
		Wiki:    w,
		Address: address,
	}

	r.Use(middleware.Heartbeat("/ping"))
	r.Use(middleware.CleanPath)
	r.Use(middleware.Recoverer)

	r.Route("/admin/", a.setupAdmin)

	return a
}

func (a *API) Run() error {
	err := a.Wiki.Initialise()
	if err != nil {
		dtrace.Println(err)
		return err
	}
	defer a.Wiki.Close()

	dtrace.Println(a.Address)

	return http.ListenAndServe(a.Address, a.r)
}

type API struct {
	Address string

	r chi.Router

	Wiki *temcore.Wiki

	Auth  temauth.Auth
	OAuth temauth.OAuth
}

func (a *API) setupAdmin(r chi.Router) {
	r.Get("/database/sync", http.HandlerFunc(a.UpdateRepo))
}

func (a *API) UpdateRepo(w http.ResponseWriter, r *http.Request) {
	err := func() error {
		res, err := a.Wiki.UpdateDatabase()
		if err != nil {
			return err
		}

		log.Println(r.RemoteAddr, "called /admin/database/sync")
		log.Println(res)

		return nil
	}()
	if err != nil {
		log.Println(err)
	}
}
