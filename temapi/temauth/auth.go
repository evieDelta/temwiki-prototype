package temauth

type AccessLevel uint

// Access levels
const (
	// The access lever everybody has regardless of having auth or not
	AccessAnon AccessLevel = iota
	// The access level of those who are authenticated
	AccessVerified
	// The access level of people who are trustworthy enough to get direct write access
	AccessContributor
	// The access level of internal moderation and administration commands
	AccessAdmin
	// The access level of owners, only functions which are to be highly restricted are here
	AccessOwner
)

// User defines a user and their access level
type User struct {
	Name      string
	IDOrEmail string

	AccessLevel int
}

// Auth contains the primary auth stuff needed to be implemented, though only having this means that all tokens will have to be made externally
type Auth interface {
	GetAnonAccount() (*User, error)
	Authenticate(name, token string) (*User, error)
}

// OAuth if implemented will allow an implementation to authenticate a user by discord oauth
type OAuth interface {
	NewUser(userid string, barer string) (*User, error)
}
