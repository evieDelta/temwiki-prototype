package temcore

import (
	"context"

	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temdb"
	"codeberg.org/eviedelta/temwiki/temfs"
)

type Wiki struct {
	DB temdb.Database

	FS *temfs.Filesystem
}

func (w *Wiki) Initialise() error {
	err := w.DB.Connect(context.TODO())
	if err != nil {
		dtrace.Println(err)
		w.DB.Close()
		return err
	}

	return nil
}

func (w *Wiki) Close() {
	w.DB.Close()
}
