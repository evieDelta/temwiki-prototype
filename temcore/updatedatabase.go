package temcore

import (
	"context"
	"os"
	"strings"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/sid"
)

// All i can say is this function is a spaghetti bowl and a half
// Thankfully it only needs to run occassionally to bring the database back to head

func (w *Wiki) UpdateDatabase() (string, error) {
	ctx := context.TODO()

	desc := ""

	com, err := w.DB.GetLastCommit(ctx)
	if err != nil {
		return desc, err
	}

	lcm, err := w.FS.LatestCommit()
	if err != nil {
		return desc, err
	}

	if com == "" {
		tags, err := w.FS.GetAllTags()
		if err != nil {
			return desc, err
		}

		err = w.DB.UpdateTags(ctx, tags, nil, nil)
		if err != nil {
			return desc, err
		}

		arts, err := w.FS.ListArticlesRecursive(temwiki.ArticlePath{})
		if err != nil {
			return desc, err
		}

		arties := make([]*temwiki.Article, 0, len(arts))

		for _, x := range arts {
			art, err := w.FS.GetArticle(x)
			if err != nil {
				return desc, err
			}

			arties = append(arties, art)
		}

		cats, err := w.FS.GetAllCategoryConfigs(temwiki.ArticlePath{}, true)
		if err != nil {
			return desc, err
		}

		arties, err = w.BakeArticleRelations(ctx, tags, cats, arties)
		if err != nil {
			return desc, err
		}

		err = w.DB.UpdateLastCommit(ctx, lcm)
		if err != nil {
			return desc, err
		}

		return desc, w.DB.UpdateArticles(ctx, arties, nil, nil)
	}

	if lcm == com {
		return "No changes", nil
	}

	r, desc, err := w.FS.ModifiedFiles(com)
	if err != nil {
		return desc, err
	}

	err = w.DB.UpdateTags(ctx, r.Tags.Created, r.Tags.Changed, r.Tags.Deleted)
	if err != nil {
		return desc, err
	}

	tagpile := append(append([]*temwiki.Tag{}, r.Tags.Created...), r.Tags.Changed...)
	catpile := append(append([]*temwiki.Category{}, r.CateConf.Created...), r.CateConf.Changed...)

	r.Articles.Changed, err = w.BakeArticleRelations(ctx, tagpile, catpile, r.Articles.Changed)
	if err != nil {
		return desc, err
	}
	r.Articles.Created, err = w.BakeArticleRelations(ctx, tagpile, catpile, r.Articles.Created)
	if err != nil {
		return desc, err
	}
	err = w.DB.UpdateArticles(ctx, r.Articles.Created, r.Articles.Changed, r.Articles.Deleted)
	if err != nil {
		return desc, err
	}

	err = w.DB.UpdateLastCommit(ctx, lcm)
	if err != nil {
		return desc, err
	}

	return desc, nil
}

func (w *Wiki) BakeArticleRelations(ctx context.Context, tags []*temwiki.Tag, categories []*temwiki.Category, articles []*temwiki.Article) ([]*temwiki.Article, error) {
	var tcache = make(map[sid.ID]*temwiki.Tag)
	var acache = make(map[string]*temwiki.Article, len(articles)*2)
	var catcache = make(map[string]*temwiki.Category, 20)

	for _, x := range articles {
		acache[x.Path.Path()] = x
	}

	for _, x := range tags {
		tcache[x.ID] = x

		arts, err := w.DB.FindAllArticlesWithTag(ctx, x.ID)
		if err != nil {
			return nil, err
		}

		for _, y := range arts {
			if acache[y.String()] != nil {
				continue
			}

			art, err := w.FS.GetArticle(y)
			if err != nil {
				return nil, err
			}

			acache[y.String()] = art
		}
	}

	for _, x := range acache {
		t := strings.Join(x.Category, ".")

		if catcache[t] != nil {
			continue
		}
		cat, err := w.FS.GetCategoryConfig(x.Category)
		if os.IsNotExist(err) {
			continue
		}
		if err != nil {
			return nil, err
		}

		catcache[t] = cat
	}

	for _, x := range acache {
		for i := range x.Category {
			if i == 0 {
				continue
			}
			t := strings.Join(x.Category[:i], ".")
			cat := catcache[t]

			if cat == nil {
				dtrace.Println("potential error here")
				continue
			}

		cattag:
			for _, z := range cat.Tags {
				for _, a := range x.Tags {
					if a == z {
						continue cattag
					}
				}
				x.Tags = append(x.Tags)
			}
		}
	}

	for _, x := range acache {
		for _, y := range x.Tags {
			t := tcache[y]

			if t == nil {
				var err error
				t, err = w.FS.GetTag(y)
				if err != nil {
					return nil, err
				}

				tcache[y] = t
			}

		tagtag:
			for _, z := range t.Relations {
				switch z.Type {
				case temwiki.RelationTypeIsAlso:
					for _, a := range x.Tags {
						if a == z.With {
							continue tagtag
						}
					}
					x.Tags = append(x.Tags)
				}
			}
		}
	}

	articles = articles[0:0] // reset articles to reuse the slice

	for _, x := range acache {
		articles = append(articles, x)
	}

	return articles, nil
}
