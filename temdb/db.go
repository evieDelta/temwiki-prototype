package temdb

import (
	"context"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
)

// Database is the main interface a search database needs to implement
type Database interface {
	Connect(ctx context.Context) error
	Close()

	GetTag(ctx context.Context, id sid.ID) (*temwiki.Tag, error)
	GetTagStringID(ctx context.Context, ids string) (*temwiki.Tag, error)
	GetTagByName(ctx context.Context, name string) (*temwiki.Tag, error)
	GetTagByNameAutocomplete(ctx context.Context, name string) ([]*temwiki.TagAutocomplete, error)
	UpdateTag(ctx context.Context, tag *temwiki.Tag) error

	GetArticleByPath(ctx context.Context, apth *temwiki.ArticlePath) (*temwiki.Article, error)
	FindAllArticlesWithTag(ctx context.Context, tag sid.ID) ([]temwiki.ArticlePath, error)
	FindArticleByName(ctx context.Context, name string) (*temwiki.Article, error)
	FindArticleByPathAutocomplete(ctx context.Context, name string) ([]string, error)
	UpdateArticle(ctx context.Context, art *temwiki.Article) error

	UpdateRepoMeta(ctx context.Context, rm *temwiki.RootMeta) error
	GetRepoMeta(ctx context.Context) (*temwiki.RootMeta, error)
	GetLastCommit(ctx context.Context) (string, error)

	UpdateTags(ctx context.Context, new, modified []*temwiki.Tag, remove []sid.ID) error
	UpdateArticles(ctx context.Context, new, modified []*temwiki.Article, remove []temwiki.ArticlePath) error
	UpdateLastCommit(ctx context.Context, hash string) error
}
