package tpsql

import (
	"context"
	"strings"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/sid"
)

func (d *Database) GetArticleByPath(ctx context.Context, apth *temwiki.ArticlePath) (*temwiki.Article, error) {
	var art = new(temwiki.Article)

	err := d.newQuery(nil).Select("*").From(tableArticles).
		SQL("WHERE "+colArticles.Path+" = ?", apth).
		QueryOne(ctx, art)

	if err != nil {
		return nil, err
	}
	return art, nil
}

func (d *Database) FindAllArticlesWithTag(ctx context.Context, tag sid.ID) ([]temwiki.ArticlePath, error) {
	var art []temwiki.ArticlePath

	err := d.newQuery(nil).Select(colArticles.Path).From(tableArticles).
		SQL("WHERE "+colArticles.Tags+" @> ?", tag).
		Query(ctx, &art)

	if err != nil {
		return nil, err
	}
	return art, nil
}

func (d *Database) FindArticleByName(ctx context.Context, name string) (*temwiki.Article, error) {
	var art = new(temwiki.Article)

	err := d.newQuery(nil).Select("*").From(tableArticles).
		SQL("WHERE "+colArticles.Name+" ILIKE ?", name).
		QueryOne(ctx, art)

	if err != nil {
		return nil, err
	}
	return art, nil
}

// FindArticleByNameAutocomplete is like FindArticleByName except it'll returns a slice of all articles with name as a prefix, can be used for autocomplete
func (d *Database) FindArticleByPathAutocomplete(ctx context.Context, name string) ([]string, error) {
	var list = []string{}

	name += "%"

	err := d.newQuery(nil).Select(colArticles.Path).From(tableArticles).
		SQL("WHERE "+colArticles.Path+" LIKE ?", name).
		Query(ctx, &list)

	if err != nil {
		return nil, err
	}
	return list, nil
}

func (d *Database) UpdateArticle(ctx context.Context, art *temwiki.Article) error {
	dtrace.Printfln("%+v", art)
	_, err := d.newQuery(nil).
		Update(tableArticles,
			colArticles.Name, colArticles.Category, colArticles.Path, colArticles.FileHash, colArticles.Tags, colArticles.Overview,
			colArticles.OverviewDescription, colArticles.Pages, colArticles.Properties, colArticles.GitMeta).
		AddValues(art.Name, art.Category, art.Path, art.FileHash, art.Tags, art.Overview, art.Overview.Description, art.Pages, art.Properties, art.GitMeta).
		SQL(`WHERE `+colArticles.Path+"=?", art.Path).
		Exec(ctx)

	return err
}

func (d *Database) UpdateArticles(ctx context.Context, new, modified []*temwiki.Article, remove []temwiki.ArticlePath) error {
	tx, err := d.conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	if len(remove) > 1 {
		itag := make([]interface{}, len(remove))
		for i, x := range remove {
			itag[i] = x
		}
		_, err = d.newQuery(tx).
			SQL("DELETE FROM "+tableArticles).
			SQL("WHERE "+colArticles.Path+" IN "+func() string { i := strings.Repeat("?, ", len(itag)); return i[:len(i)-2] }()+")", itag...).
			Exec(ctx)

		if err != nil {
			return err
		}
	}

	for _, art := range modified {
		_, err := d.newQuery(tx).
			Update(tableArticles,
				colArticles.Name, colArticles.Category, colArticles.Path, colArticles.FileHash, colArticles.Tags, colArticles.Overview,
				colArticles.OverviewDescription, colArticles.Pages, colArticles.Properties, colArticles.GitMeta).
			AddValues(art.Name, art.Category, art.Path, art.FileHash, art.Tags, art.Overview, art.Overview.Description, art.Pages, art.Properties, art.GitMeta).
			SQL(`WHERE `+colArticles.Path+"=?", art.Path).
			Exec(ctx)

		if err != nil {
			return err
		}
	}
	for _, art := range new {
		_, err := d.newQuery(tx).
			InsertInto(tableArticles,
				colArticles.Name, colArticles.Category, colArticles.Path, colArticles.FileHash, colArticles.Tags, colArticles.Overview,
				colArticles.OverviewDescription, colArticles.Pages, colArticles.Properties, colArticles.GitMeta).
			Values(art.Name, art.Category, art.Path, art.FileHash, art.Tags, art.Overview, art.Overview.Description, art.Pages, art.Properties, art.GitMeta).
			Exec(ctx)

		if err != nil {
			return err
		}
	}

	return tx.Commit(ctx)
}
