package tpsql

const (
	tableInternalMeta = "internal_meta"
	tableTags         = "tags"
	tableArticles     = "articles"
)

// using structs for namespacing and typesafety of sql columns
var colInternalMeta = struct {
	Key, Value string
}{
	Key:   "key",
	Value: "value",
}

var keyInternalMeta = struct {
	SchemaVersion, LastCommit, RepoMeta string
}{
	SchemaVersion: "schema_version",
	LastCommit:    "last_commit",
	RepoMeta:      "repo_meta",
}

var colTags = struct {
	ID, Name, Description, Properties, Notes, Relations string
}{
	ID:          "id",
	Name:        "name",
	Description: "description",
	Properties:  "properties",
	Notes:       "notes",
	Relations:   "relations",
}

var colArticles = struct {
	Name, Category, Path, FileHash, Tags, Overview, OverviewDescription, Pages, PagesDescriptions, Properties, GitMeta string
}{
	Name:                "name",
	Category:            "category",
	Path:                "path",
	FileHash:            "filehash",
	Tags:                "tags",
	Overview:            "overview",
	OverviewDescription: "overview_description",
	Pages:               "pages",
	PagesDescriptions:   "pages_descriptions",
	Properties:          "properties",
	GitMeta:             "git_meta",
}
