// csqlb is a really crappy query builder library
package csqlb

import (
	"context"
	"strconv"
	"strings"

	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

type Querier interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
}

// New returns a new query
func New(p Querier) *Query {
	return &Query{
		conq: p,
	}
}

// Query is a query builder
type Query struct {
	s strings.Builder

	args []interface{}

	conq Querier
}

func (q *Query) Finish() (string, []interface{}) {
	return q.s.String(), q.args
}

func (a *Query) string() string {
	return placeholderise(a.s.String()) + ";"
}

func placeholderise(s string) string {
	b := strings.Builder{}
	index := 1
	for {
		i := strings.Index(s, "?")
		if i == -1 {
			b.WriteString(s)
			return b.String()
		}
		b.WriteString(s[:i])
		s = s[i+1:]

		b.WriteString("$")
		b.WriteString(strconv.Itoa(index))

		index++
	}
}

func (q *Query) Exec(ctx context.Context) (pgconn.CommandTag, error) {
	dtrace.Println(q.string(), "\n", q.args)
	return q.conq.Exec(ctx, q.string(), q.args...)
}

func (q *Query) Query(ctx context.Context, val interface{}) error {
	dtrace.Println(q.string(), "\n", q.args)
	rows, err := q.conq.Query(ctx, q.string(), q.args...)
	if err != nil {
		return err
	}

	return pgxscan.ScanAll(val, rows)
}

func (q *Query) QueryOne(ctx context.Context, val interface{}) error {
	dtrace.Println(q.string(), "\n", q.args)
	rows, err := q.conq.Query(ctx, q.string(), q.args...)
	if err != nil {
		return err
	}

	return pgxscan.ScanOne(val, rows)
}

func (q *Query) space() *Query {
	q.s.WriteRune(' ')
	return q
}

// SQL just lets you insert raw SQL
func (q *Query) SQL(sql string, args ...interface{}) *Query {
	q.s.WriteString(sql)
	q.args = append(q.args, args...)
	return q.space()
}

// Select appends `SELECT ` + what to query, if no arguments are given it defaults to *
func (q *Query) Select(what ...string) *Query {
	q.s.WriteString("SELECT ")
	if len(what) == 1 {
		q.s.WriteString(what[0])
	} else if len(what) > 1 {
		q.space()
		q.s.WriteRune('(')
		q.s.WriteString(strings.Join(what, ", "))
		q.s.WriteRune(')')
	} else {
		q.s.WriteString("*")
	}
	return q.space()
}

// From appends a `FROM ` + where to query
func (q *Query) From(where string) *Query {
	q.s.WriteString("FROM ")
	q.s.WriteString(where)
	return q.space()
}

// InsertInto adds `INSERT INTO ` + table + optionally a list of columns in brackets
func (q *Query) InsertInto(table string, columns ...string) *Query {
	q.s.WriteString("INSERT INTO ")
	q.s.WriteString(table)
	if len(columns) > 0 {
		q.space()
		q.s.WriteRune('(')
		q.s.WriteString(strings.Join(columns, ", "))
		q.s.WriteRune(')')
	}
	return q.space()
}

// Values is used for InsertInto, it ads `VALUES ` + a number of arguments
func (q *Query) Values(args ...interface{}) *Query {
	q.s.WriteString("VALUES ")
	q.s.WriteRune('(')
	q.s.WriteString(func() string { i := strings.Repeat("?, ", len(args)); return i[:len(i)-2] }())
	q.s.WriteRune(')')
	return q.AddValues(args...).space()
}

// Update adds `UPDATE ` + table + a list of columns, use AddValues to add the variables to the query (this is a requirement because you can't have more than one ...type in a function signature)
func (q *Query) Update(table string, columns ...string) *Query {
	q.s.WriteString("UPDATE ")
	q.s.WriteString(table)
	if len(columns) > 0 {
		q.s.WriteString(" SET ")
		q.s.WriteString(strings.Join(columns, " = ?, "))
		q.s.WriteString(" = ?")
	}
	return q.space()
}

// AddValues adds values to the arg stack, mostly just used for Update
func (q *Query) AddValues(args ...interface{}) *Query {
	q.args = append(q.args, args...)
	return q
}
