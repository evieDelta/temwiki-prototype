package tpsql

import (
	"context"
	"encoding/json"
	"strconv"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/temdb/tpsql/internal/csqlb"
)

func (d *Database) GetRepoMeta(ctx context.Context) (*temwiki.RootMeta, error) {
	var rm = new(temwiki.RootMeta)

	var jtext string

	err := d.newQuery(nil).Select("*").From(tableInternalMeta).
		SQL("WHERE "+colInternalMeta.Key+" = ?", keyInternalMeta.RepoMeta).
		QueryOne(ctx, jtext)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(jtext), rm)

	return rm, nil
}

func (d *Database) UpdateRepoMeta(ctx context.Context, rm *temwiki.RootMeta) error {
	jtext, err := json.Marshal(rm)
	if err != nil {
		return err
	}

	_, err = d.newQuery(nil).
		Update(tableInternalMeta, colInternalMeta.Value).
		Values(string(jtext)).
		SQL(`WHERE `+colInternalMeta.Key+" = ?", keyInternalMeta.RepoMeta).
		Exec(ctx)

	return err
}

func (d *Database) GetLastCommit(ctx context.Context) (string, error) {
	var str string

	err := d.newQuery(nil).Select(colInternalMeta.Value).From(tableInternalMeta).
		SQL("WHERE "+colInternalMeta.Key+" = ?", keyInternalMeta.LastCommit).
		QueryOne(ctx, &str)

	return str, err
}

func (d *Database) UpdateLastCommit(ctx context.Context, s string) error {
	_, err := d.newQuery(nil).
		Update(tableInternalMeta, colInternalMeta.Value).
		AddValues(s).
		SQL(`WHERE `+colInternalMeta.Key+" = ?", keyInternalMeta.LastCommit).
		Exec(ctx)

	return err
}

func (d *Database) getDatabaseSchemaVersion(ctx context.Context) (int, error) {
	var sver string

	err := d.newQuery(nil).Select(colInternalMeta.Value).From(tableInternalMeta).
		SQL("WHERE "+colInternalMeta.Key+" = ?", keyInternalMeta.SchemaVersion).
		QueryOne(ctx, &sver)

	if err != nil {
		return 0, err
	}

	return strconv.Atoi(sver)
}

func (d *Database) updateDatabaseSchemaVersion(ctx context.Context, tx csqlb.Querier, ver int) error {
	_, err := d.newQuery(tx).
		Update(tableInternalMeta, colInternalMeta.Value).
		AddValues(strconv.Itoa(ver)).
		SQL(`WHERE `+colInternalMeta.Key+" = ?", keyInternalMeta.SchemaVersion).
		Exec(ctx)

	return err
}
