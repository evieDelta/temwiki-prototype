package tpsql

import (
	"embed"
	"fmt"
	"io"
	"io/fs"
	"sort"
	"strconv"
	"strings"
)

//go:embed sql/*
var migrationFiles embed.FS

func init() {
	// i'm surprised i never realised you could do this until now
	type file struct {
		n int
		f fs.DirEntry
	}
	const pref = "sql"

	dir, err := migrationFiles.ReadDir(pref)
	if err != nil {
		panic(err)
	}
	for _, x := range dir {
		fmt.Println(x.Name(), x.IsDir(), x.Type().String())
	}

	// preprocessing for the sort so it doesn't need to reprocess the numbers potentially several times
	// or rely on the strings sorting correctly
	// ( the files should all be named as 00#.sql or maybe 0##.sql in the future
	//   and those should sort correctly but just in case we are not putting our sole trust in the strings
	//   and are instead taking the initiative to use their actual numarical value to sort by instead )
	var ford = make([]file, 0, len(dir))
	for _, x := range dir {
		if x.IsDir() {
			continue // just in case we'll avoid directories since this is easy enough
		}
		n := strings.SplitN(x.Name(), ".", 2)[0] // if the len is 0 something is seriously wrong so we don't bother checking (we'll put out a panic regardless here)
		// ( it should always be 2 assuming all files are named by ###.sql
		//   it could be 1 if somebody did something wrong but thats not really a big deal since we ignore the extension
		//   however it should never ever be 0 for as long as the OS and compiler is working properly )

		// parse the number and append it to the list
		i, err := strconv.Atoi(n)
		if err != nil {
			// all the files should be named by a number followed by the .sql extension
			// if its not a valid number something is wrong either in the compiler or the source
			panic(err)
		}
		ford = append(ford, file{n: i, f: x})
	}

	// just as a precaution to make sure everything is in the correct order
	// since the order does matter when we are just putting it in the migration array whatever order the array is
	sort.Slice(ford, func(i, j int) bool { return ford[i].n < ford[j].n })

	// read all the embedded sql files and add them to the migrations array
	for _, x := range ford {
		f, err := migrationFiles.Open(pref + "/" + x.f.Name())
		if err != nil {
			// considering these files are embedded if something can't be found
			// inbetween getting the dir info and here something is *seriously* wrong
			panic(err)
		}

		d, err := io.ReadAll(f)
		if err != nil {
			// if it can't read the file data itself something is also seriously wrong
			// and that thing must be investigated before we allow the program to run.
			panic(err)
		}

		// and we add the data to the migration list
		migrations = append(migrations, string(d))
	}
}

var migrations = []string{}
