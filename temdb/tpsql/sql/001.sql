CREATE TABLE public.articles (
	name                 text      COLLATE pg_catalog."default" NOT NULL,
	category             text[]    COLLATE pg_catalog."default" NOT NULL,
	path                 text[]    COLLATE pg_catalog."default" NOT NULL,
	filehash             text      COLLATE pg_catalog."default" NOT NULL,
	tags                 bigint[],
	overview             jsonb,
	overview_description tsvector,
	pages                jsonb,
	properties           jsonb     NOT NULL,
	git_meta             jsonb,

	CONSTRAINT articles_pkey PRIMARY KEY (path)
);

CREATE TABLE public.internal_meta (
    key   text COLLATE pg_catalog."default" NOT NULL,
	value text                              NOT NULL,
	
    CONSTRAINT internal_meta_pkey PRIMARY KEY (key)
);

CREATE TABLE public.tags (
    id          bigint                              NOT NULL,
    name        text   COLLATE pg_catalog."default" NOT NULL,
    description text   COLLATE pg_catalog."default" NOT NULL,
    properties  jsonb                               NOT NULL,
    notes       jsonb                               NOT NULL,
	relations   jsonb,
	
    CONSTRAINT tags_pkey PRIMARY KEY (id)
);

CREATE UNIQUE INDEX autocomplete
    ON public.tags USING btree
    (name COLLATE pg_catalog."default" text_pattern_ops ASC NULLS LAST)
    INCLUDE(id, description)
    TABLESPACE pg_default;

INSERT INTO internal_meta (key, value) VALUES (
	'schema_version',
	'0'
);
INSERT INTO internal_meta (key, value) VALUES (
	'last_commit',
	''
);
INSERT INTO internal_meta (key, value) VALUES (
	'repo_meta',
	''
);