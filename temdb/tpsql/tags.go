package tpsql

import (
	"context"
	"encoding/json"
	"strings"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
)

func (d *Database) GetTag(ctx context.Context, id sid.ID) (*temwiki.Tag, error) {
	var tag = new(temwiki.Tag)

	err := d.newQuery(nil).Select("*").From(tableTags).
		SQL("WHERE "+colTags.ID+" = ?", id).
		QueryOne(ctx, tag)

	if err != nil {
		return nil, err
	}
	return tag, nil
}

func (d *Database) GetTagStringID(ctx context.Context, ids string) (*temwiki.Tag, error) {
	id, err := sid.ParseIDString(ids)
	if err != nil {
		return nil, err
	}
	return d.GetTag(ctx, id)
}

func (d *Database) GetTagByName(ctx context.Context, name string) (*temwiki.Tag, error) {
	var tag = new(temwiki.Tag)

	err := d.newQuery(nil).Select("*").From(tableTags).
		SQL("WHERE "+colTags.Name+" ILIKE ?", name).
		QueryOne(ctx, tag)

	if err != nil {
		return nil, err
	}
	return tag, nil
}

// GetTagByNameAutocomplete is like GetTagByName except it'll returns a slice of all tags with name as a prefix, can be used for autocomplete
func (d *Database) GetTagByNameAutocomplete(ctx context.Context, name string) ([]*temwiki.TagAutocomplete, error) {
	var tags = []*temwiki.TagAutocomplete{}

	name += "%"

	err := d.newQuery(nil).Select(colTags.Name, colTags.ID).From(tableTags).
		SQL("WHERE "+colTags.Name+" LIKE ?", name).
		Query(ctx, tags)

	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (d *Database) UpdateTag(ctx context.Context, tag *temwiki.Tag) error {
	notes, err := json.Marshal(tag.Notes)
	if err != nil {
		return err
	}
	properties, err := json.Marshal(tag.Properties)
	if err != nil {
		return err
	}
	relations, err := json.Marshal(tag.Relations)
	if err != nil {
		return err
	}
	_, err = d.newQuery(nil).
		Update(tableTags,
			colTags.ID, colTags.Description, colTags.Name, colTags.Notes, colTags.Properties, colTags.Relations).
		Values(tag.ID, tag.Description, tag.Name, string(notes), string(properties), string(relations)).
		SQL(`WHERE `+colTags.ID+"=?", tag.ID).
		Exec(ctx)

	return err
}

func (d *Database) UpdateTags(ctx context.Context, new, modified []*temwiki.Tag, remove []sid.ID) error {
	tx, err := d.conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	if len(remove) > 1 {
		itag := make([]interface{}, len(remove))
		for i, x := range remove {
			itag[i] = x
		}

		_, err = d.newQuery(tx).
			SQL("DELETE FROM "+tableTags).
			SQL("WHERE "+colTags.ID+" IN ("+func() string { i := strings.Repeat("?, ", len(itag)); return i[:len(i)-2] }()+")", itag...).
			Exec(ctx)

		if err != nil {
			return err
		}
	}

	if len(modified) > 0 {
		for _, x := range modified {
			notes, err := json.Marshal(x.Notes)
			if err != nil {
				return err
			}
			properties, err := json.Marshal(x.Properties)
			if err != nil {
				return err
			}
			relations, err := json.Marshal(x.Relations)
			if err != nil {
				return err
			}
			_, err = d.newQuery(tx).
				Update(tableTags,
					colTags.ID, colTags.Description, colTags.Name, colTags.Notes, colTags.Properties, colTags.Relations).
				AddValues(x.ID, x.Description, x.Name, string(notes), string(properties), string(relations)).
				SQL(`WHERE `+colTags.ID+"=?", x.ID).
				Exec(ctx)

			if err != nil {
				return err
			}
		}
	}

	if len(new) > 0 {
		for _, x := range new {
			notes, err := json.Marshal(x.Notes)
			if err != nil {
				return err
			}
			properties, err := json.Marshal(x.Properties)
			if err != nil {
				return err
			}
			relations, err := json.Marshal(x.Relations)
			if err != nil {
				return err
			}
			_, err = d.newQuery(tx).
				InsertInto(tableTags,
					colTags.ID, colTags.Description, colTags.Name, colTags.Notes, colTags.Properties, colTags.Relations).
				Values(x.ID, x.Description, x.Name, string(notes), string(properties), string(relations)).
				Exec(ctx)

			if err != nil {
				return err
			}
		}
	}

	return tx.Commit(ctx)
}
