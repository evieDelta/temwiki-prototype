package tpsql

import (
	"context"
	"time"

	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temdb/tpsql/internal/csqlb"
	"github.com/jackc/pgx/v4/pgxpool"
)

func New(connstring string) *Database {
	return &Database{
		connstring: connstring,
	}
}

func (d *Database) Connect(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*3000)
	defer cancel()

	conf, err := pgxpool.ParseConfig(d.connstring)
	if err != nil {
		return err
	}

	pool, err := pgxpool.ConnectConfig(ctx, conf)
	if err != nil {
		return err
	}

	d.conn = pool

	con, err := pool.Acquire(ctx)
	if err != nil {
		return err
	}
	defer con.Release()

	ver, err := d.getDatabaseSchemaVersion(ctx)
	if err != nil {
		dtrace.Println(err)
	}

	if ver < len(migrations) || err != nil {
		tx, err := pool.Begin(ctx)
		if err != nil {
			return err
		}
		defer func() {
			dtrace.Println("rolling back", tx.Rollback(ctx))
		}()
		dtrace.Println(ver, len(migrations))
		for i := ver; i < len(migrations); i++ {
			dtrace.Println(i, len(migrations))
			t, err := tx.Exec(ctx, migrations[i])
			if err != nil {
				dtrace.Println(err)
				return err
			}
			dtrace.Println(t)
		}
		err = d.updateDatabaseSchemaVersion(ctx, tx, len(migrations))
		if err != nil {
			return err
		}
		err = tx.Commit(ctx)
		if err != nil {
			dtrace.Println(err)
			return err
		}
	}

	return con.Conn().Ping(ctx)
}

func (d *Database) Close() {
	dtrace.Println("closing")
	d.conn.Close()
	dtrace.Println("closed")
}

type Database struct {
	connstring string

	conn *pgxpool.Pool
}

func (d *Database) newQuery(tx csqlb.Querier) *csqlb.Query {
	if tx == nil {
		return csqlb.New(d.conn)
	}

	return csqlb.New(tx)
}
