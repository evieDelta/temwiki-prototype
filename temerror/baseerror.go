package temerror

// Cause is a regular old error type
type Cause string

func (c Cause) Error() string {
	return string(c)
}
