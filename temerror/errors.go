package temerror

import (
	"fmt"
	"path"
	"runtime"
	"strconv"
	"strings"
)

var baseDir string

func init() {
	_, f, _, ok := runtime.Caller(0)
	if ok {
		baseDir = strings.TrimSuffix(path.Dir(f), "temerror")
	}
}

// ErrorWithContext is an error but fancier, it encodes what line and file the error was created at
type ErrorWithContext struct {
	Cause       error
	Description string

	HasFileAndLine bool
	File           string
	Line           int
}

func (t ErrorWithContext) Error() string {
	b := strings.Builder{}
	b.Grow(100)
	if t.HasFileAndLine {
		b.WriteString(t.File)
		b.WriteString(":")
		b.WriteString(strconv.Itoa(t.Line))
		b.WriteString(": ")
	}
	b.WriteString(t.Description)
	if t.Cause != nil {
		b.WriteString(": ")
		b.WriteString(t.Cause.Error())
	}

	return b.String()
}

func (t ErrorWithContext) Unwrap() error {
	return t.Cause
}

// New returns a new error with context
func New(cause error, des string) error {
	_, f, i, ok := runtime.Caller(1)

	return ErrorWithContext{
		Cause:       cause,
		Description: des,

		HasFileAndLine: ok,
		File:           strings.TrimPrefix(f, baseDir),
		Line:           i,
	}
}

// Newf returns a new error with context using fmt.Sprintf logic for creating the description
func Newf(cause error, f string, d ...interface{}) error {
	return New(cause, fmt.Sprintf(f, d...))
}
