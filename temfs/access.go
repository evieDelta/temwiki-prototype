package temfs

import (
	"io/ioutil"
	"os"
	"path"
	"sync"
	"time"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temfs/temconfig"
	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
)

// Accessor contains some low level functions used for accessing files and git info
type Accessor struct {
	opts *Options
	*accessorCommon

	readOnly bool

	sync.RWMutex
}

// AllowWriting tells you if its allowed to write, and returns ErrorWritingDisallowed if its not
func (a *Accessor) AllowWriting() error {
	if a.readOnly {
		return ErrWritingDisallowed
	}
	return nil
}

type accessorCommon struct {
	repo *git.Repository

	root billy.Filesystem

	rmet temwiki.RootMeta

	Conf temconfig.Loader
}

// ReadDirectoryContents returns a lite version of fileinfo specifying all files in a folder
func (a *Accessor) ReadDirectoryContents(dir string) ([]os.FileInfo, error) {
	fi, err := a.root.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	fl := make([]os.FileInfo, len(fi))
	for i, x := range fi {
		fl[i] = x
	}

	return fl, nil
}

// ReadFile reads any kind of file from anywhere and not just structured config types
// note that this is not locking
func (a *Accessor) ReadFile(file string) ([]byte, error) {
	var flags int

	flags |= os.O_RDONLY
	flags |= os.O_EXCL

	rd, err := a.root.OpenFile(file, flags, 0664)
	if err != nil {
		return nil, err
	}
	defer rd.Close()

	return ioutil.ReadAll(rd)
}

// FileStat returns info about a file
func (a *Accessor) FileStat(file string) (os.FileInfo, error) {
	return a.root.Stat(file)
}

// WriteFile writes to any kind of file anywhere
// be aware that write config is a lowlevel function and that it does not commit the file automatically, it is recommended to use transactions for most write actions
// note that this also does not lock the state
func (a *Accessor) WriteFile(file string, data []byte) error {
	if err := a.AllowWriting(); err != nil {
		return err
	}

	var flags int

	rd, err := a.root.Create(file)
	if err != nil {
		// if it doesn't exist it might be lacking the directory, so try adding them
		if os.IsExist(err) {
			err = a.root.MkdirAll(path.Dir(file), 0664)
			if err != nil {
				return err
			}
			rd, err = a.root.OpenFile(file, flags, 0664)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	defer rd.Close()

	_, err = rd.Write(data)
	return err
}

// DeleteFile deletes a file
func (a *Accessor) DeleteFile(file string) error {
	return a.root.Remove(file)
}

// CommitFile commits a file to the git repo with `who` as the author specification and signed with the key in a.Opts.PGPKey,
// If who is nil it'll do nothing and return with no error
func (a *Accessor) CommitFile(who *Commit, file string) error {
	if who == nil {
		return nil
	}

	a.Lock()
	defer a.Unlock()

	wt, err := a.repo.Worktree()
	if err != nil {
		return err
	}

	h, err := wt.Add(file)
	if err != nil {
		return err
	}
	dtrace.Println(h)

	copts := &git.CommitOptions{
		SignKey: a.opts.PGPKey,
	}

	if !who.UseConfig {
		copts.Author = &object.Signature{
			Name:  who.Name,
			Email: who.EmailID,

			When: time.Now().UTC(),
		}
	}

	h, err = wt.Commit(who.Why, copts)
	if err != nil {
		return err
	}

	dtrace.Println(h)

	return nil
}

// GetFileHash gets and returns the git hash of a file at a perticular location
func (a *Accessor) GetFileHash(file string) (plumbing.Hash, error) {
	dat, err := a.ReadFile(file)
	if err != nil {
		return plumbing.ZeroHash, err
	}
	return Hash(dat), nil
}

func (a *Accessor) GetModificationMeta(file string) (meta temwiki.GitMeta, err error) {
	l, err := a.repo.Log(&git.LogOptions{
		PathFilter: func(s string) bool {
			return s == file
		},
		Order: git.LogOrderCommitterTime,
	})
	if err != nil {
		return meta, err
	}

	var created, modified plumbing.Hash

	err = l.ForEach(func(c *object.Commit) error {
		dtrace.Println(c.String())

		// assume the last time this is ran is the oldest commit
		meta.Created.When = c.Author.When
		meta.Created.CommitHash = c.Hash.String()
		meta.Created.By.Name = c.Author.Name
		meta.Created.By.EmailID = c.Author.Email
		meta.Created.PGPSignature = c.PGPSignature
		created = c.Hash

		if meta.Modified.When.IsZero() || meta.Modified.When.Before(c.Author.When) {
			meta.Modified.When = c.Author.When
			meta.Modified.CommitHash = c.Hash.String()
			meta.Modified.By.Name = c.Author.Name
			meta.Modified.By.EmailID = c.Author.Email
			meta.Modified.PGPSignature = c.PGPSignature
			modified = c.Hash
		}

		return nil
	})
	if err != nil {
		return meta, err
	}

	if meta.Created.PGPSignature != "" {
		valid, err := a.IsVerified(created)
		if err != nil {
			return meta, err
		}
		meta.Created.Verified = valid
	}

	if meta.Modified.PGPSignature != "" {
		valid, err := a.IsVerified(modified)
		if err != nil {
			return meta, err
		}
		meta.Created.Verified = valid
	}

	return meta, err
}

func (a *Accessor) IsVerified(commit plumbing.Hash) (bool, error) {
	c, err := a.repo.CommitObject(commit)
	if err != nil {
		return false, err
	}

	if c.PGPSignature == "" {
		return false, nil
	}
	if a.opts.ValidKeys == nil {
		return false, nil
	}

	ps, err := crypto.NewPGPSignatureFromArmored(c.PGPSignature)
	if err != nil {
		return false, err
	}

	encoded := &plumbing.MemoryObject{}
	if err := c.EncodeWithoutSignature(encoded); err != nil {
		return false, err
	}
	er, err := encoded.Reader()
	if err != nil {
		return false, err
	}

	era, err := ioutil.ReadAll(er)
	if err != nil {
		return false, err
	}

	msg := crypto.NewPlainMessage(era)

	err = a.opts.ValidKeys.VerifyDetached(msg, ps, c.Author.When.Unix())
	if err != nil {
		dtrace.Println(err)
		return false, nil
	}

	// it can only get to here if it is valid
	return true, nil
}

func Hash(data []byte) plumbing.Hash {
	return plumbing.ComputeHash(plumbing.BlobObject, data)
}
