package temfs

import (
	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
)

// GetArticle fetches an article either from the file system or transaction state if given
func (f *Filesystem) GetArticle(art temwiki.ArticlePath) (*temwiki.Article, error) {
	a := temwiki.ArticleConfig{}

	loc := pathArticle(art) + f.A.Conf.FileExtension()

	err := f.A.ReadConfig(loc, &a)
	if err != nil {
		return nil, err
	}

	meta, err := f.A.GetModificationMeta(loc)
	if err != nil {
		return nil, err
	}
	hash, err := f.A.GetFileHash(loc)
	if err != nil {
		return nil, err
	}

	dtrace.Println(art.Name(), art.Categories(), art)

	return &temwiki.Article{
		Name:     art.Name(),
		Category: art.Categories(),
		Path:     art,

		ArticleConfig: a,
		GitMeta:       meta,
		FileHash:      hash.String(),
	}, err
}

// ListArticlesAndCategories lists all articles and categories within in a category
func (f *Filesystem) ListArticlesAndCategories(category temwiki.ArticlePath) (articles []temwiki.ArticlePath, subcategories []temwiki.ArticlePath, err error) {
	return f.A.ReadCategoryConfigContents(pathArticle(category))
}

// ListArticles lists all articles within a category
func (f *Filesystem) ListArticles(category temwiki.ArticlePath) (articles []temwiki.ArticlePath, err error) {
	articles, _, err = f.A.ReadCategoryConfigContents(pathArticle(category))
	return articles, err
}

// ListArticlesRecursive recursively lists all articles contained in a category including those in subcategories,
func (f *Filesystem) ListArticlesRecursive(category temwiki.ArticlePath) ([]temwiki.ArticlePath, error) {
	arts, cats, err := f.ListArticlesAndCategories(category)
	if err != nil {
		return nil, err
	}

	for _, x := range cats {
		subarts, err := f.ListArticlesRecursive(x)
		if err != nil {
			return nil, err
		}
		arts = append(arts, subarts...)
	}

	return arts, nil
}

// SaveArticle saves an article to location p and commits it if who is not nil
func (f *Filesystem) SaveArticle(who *Commit, art temwiki.Article) error {
	return f.A.WriteConfig(who, pathArticle(art.Path), art.ArticleConfig)
}
