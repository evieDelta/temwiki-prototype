package temfs

import (
	"codeberg.org/eviedelta/temwiki"
)

// GetCategoryConfig reads the !category.conf file of a directory into val
func (f *Filesystem) GetCategoryConfig(c temwiki.ArticlePath) (*temwiki.Category, error) {
	loc := pathArticle(c)

	stat, err := f.A.FileStat(loc)
	if err != nil {
		return nil, err
	}
	if !stat.IsDir() {
		return nil, ErrNotADirectory
	}

	val := temwiki.CategoryConfig{}

	err = f.A.ReadConfig(pathCategoryConfig(c), &val)
	if err != nil {
		return nil, err
	}

	return &temwiki.Category{
		Name: c,

		CategoryConfig: val,
	}, nil
}

// ListCategories lists all categories within another category
func (f *Filesystem) ListCategories(category temwiki.ArticlePath) (categories []temwiki.ArticlePath, err error) {
	_, categories, err = f.A.ReadCategoryConfigContents(pathArticle(category))
	return categories, err
}

// ListCategoriesRecursive gets a list of all categories within a category and all its child categories
func (f *Filesystem) ListCategoriesRecursive(category temwiki.ArticlePath) ([]temwiki.ArticlePath, error) {
	cats, err := f.ListCategories(category)
	if err != nil {
		return nil, err
	}

	for _, x := range cats {
		subcats, err := f.ListCategoriesRecursive(x)
		if err != nil {
			return nil, err
		}
		cats = append(subcats, subcats...)
	}

	return cats, nil
}

func (f *Filesystem) GetAllCategoryConfigs(category temwiki.ArticlePath, recursive bool) ([]*temwiki.Category, error) {
	var cats []temwiki.ArticlePath
	var err error
	if recursive {
		cats, err = f.ListCategoriesRecursive(category)
	} else {
		cats, err = f.ListCategories(category)
	}

	if err != nil {
		return nil, err
	}

	var ccon = make([]*temwiki.Category, 0, len(cats))
	for _, x := range cats {
		cat, err := f.GetCategoryConfig(x)
		if err != nil {
			return nil, err
		}
		ccon = append(ccon, cat)
	}

	return ccon, nil
}

func (f *Filesystem) WriteCateoryConfig(who *Commit, cat temwiki.Category) error {
	return f.A.WriteConfig(who, pathCategoryConfig(cat.Name), cat.CategoryConfig)
}
