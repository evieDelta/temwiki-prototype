package temfs

// GetCustomConfig loads a configuration file from the custom folder with x name and unmarshals it into y
// This is primarily intended to be used for something hosting this to be able to carry custom configuration
// such as specifying custom aliases in a bot, or custom api routes in an api
func (f *Filesystem) GetCustomConfig(name string, y interface{}) error {
	return f.A.ReadConfig(pathCustomConfig(name), y)
}

func (f *Filesystem) WriteCustomConfig(who *Commit, name string, y interface{}) error {
	return f.A.WriteConfig(who, pathCustomConfig(name), y)
}
