package temfs

import (
	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
	"codeberg.org/eviedelta/temwiki/temerror"
)

// GetTag finds and fetches a tag
func (f *Filesystem) GetTag(id sid.ID) (*temwiki.Tag, error) {
	t := temwiki.TagConfig{}

	loc := pathTagConfig(id)

	err := f.A.ReadConfig(loc, &t)
	if err != nil {
		return nil, err
	}

	return &temwiki.Tag{
		TagConfig: t,
		ID:        id,
	}, nil
}

// GetTagString finds and fetches a tag but uses a string as the ID
func (f *Filesystem) GetTagString(id string) (*temwiki.Tag, error) {
	t := temwiki.TagConfig{}

	iid, err := sid.ParseIDString(id)
	if err != nil {
		return nil, err
	}

	loc := pathTagConfigString(id)

	err = f.A.ReadConfig(loc, &t)
	if err != nil {
		return nil, err
	}

	return &temwiki.Tag{
		TagConfig: t,
		ID:        iid,
	}, nil
}

func (f *Filesystem) ListTags() ([]sid.ID, error) {
	// don't need directories at this point so ignore it with _
	confs, _, err := f.A.ReadDirectoryConfigContents(DirectoryTags)
	if err != nil {
		return nil, err
	}

	tags := make([]sid.ID, len(confs))

	for i, x := range confs {
		id, err := sid.ParseIDString(x)
		if err != nil {
			return nil, temerror.Newf(err, "illegal file in tags folder")
		}
		tags[i] = id
	}

	return tags, nil
}

// GetAllTags grabs every single tag in the tags folder, might be a little slow if there are a lot of tags
func (f *Filesystem) GetAllTags() ([]*temwiki.Tag, error) {
	// don't need directories at this point so ignore it with _
	confs, _, err := f.A.ReadDirectoryConfigContents(DirectoryTags)
	if err != nil {
		return nil, err
	}

	tags := make([]*temwiki.Tag, len(confs))

	for i, x := range confs {
		id, err := sid.ParseIDString(x)
		if err != nil {
			return nil, temerror.Newf(err, "illegal file in tags folder")
		}
		tag, err := f.GetTag(id)
		if err != nil {
			return nil, temerror.New(err, "")
		}
		tags[i] = tag
	}

	return tags, nil
}

// WriteTag saves a tag to the filesystem, and commits it if who is not nil
func (f *Filesystem) WriteTag(who *Commit, tag temwiki.Tag) error {
	return f.A.WriteConfig(who, pathTagConfig(tag.ID), tag.TagConfig)
}
