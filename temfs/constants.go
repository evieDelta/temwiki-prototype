package temfs

import (
	"path"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
)

const (
	ConfigBuiltinPrefix = "!"
)

// Built in system Directories
const (
	DirectoryTags     = "tags"
	DirectoryArticles = "articles"
	DirectoryConfigs  = "configs"
)

// Some default recognised files
const (
	// Some global repository configuration
	ConfigRepository   = "repository"
	ConfigCategoryData = ConfigBuiltinPrefix + "category"
)

// Path generation functions

// PathArticle returns an absolute path to an article
var pathArticle = func(a temwiki.ArticlePath) string { return path.Join(DirectoryArticles, a.Path()) }

// PathCategoryConfig returns a path to a category config file
var pathCategoryConfig = func(a temwiki.ArticlePath) string { return path.Join(DirectoryArticles, a.Path(), ConfigCategoryData) }

// PathCustomConfig returns a path to a custom config
var pathCustomConfig = func(file string) string { return path.Join(DirectoryConfigs, file) }

// PathTagConfig returns a path to a tag
var pathTagConfig = func(file sid.ID) /**/ string { return path.Join(DirectoryTags, file.String()) }
var pathTagConfigString = func(tag string) string { return path.Join(DirectoryTags, tag) }

// gofmt why u no align

// (is there any difference between defining functions as functions or variables?)
