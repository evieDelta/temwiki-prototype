package temfs

import "codeberg.org/eviedelta/temwiki/temerror"

// Errors :D always fun to get these
const (
	ErrIncomplete         temerror.Cause = "This function has not been written yet"
	ErrWritingDisallowed  temerror.Cause = "Writing is not allowed by settings or by this method"
	ErrNotADirectory      temerror.Cause = "Target is not a directory/category"
	ErrNoRepositoryConfig temerror.Cause = "Could not find repository configuration file, either location is not a wiki or its corrupt"
)
