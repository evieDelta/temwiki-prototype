package temfs

import (
	"io/ioutil"
	"path"
	"strings"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/temfs/temconfig"
	"codeberg.org/eviedelta/temwiki/temfs/temconfig/configreader"
	"github.com/go-git/go-git/v5"
)

func New(dir string, noWrite bool, opts *Options) (*Filesystem, error) {
	repo, err := git.PlainOpen(dir)
	if err != nil {
		return nil, err
	}
	return NewFromRepo(noWrite, opts, repo)
}

func NewFromRepo(noWrite bool, opts *Options, repo *git.Repository) (*Filesystem, error) {
	wt, err := repo.Worktree()
	if err != nil {
		return nil, err
	}

	var ext string

	{
		d, err := wt.Filesystem.ReadDir("./")
		if err != nil {
			return nil, err
		}

		for _, x := range d {
			if !strings.HasPrefix(x.Name(), ConfigRepository+".") || x.IsDir() {
				continue
			}
			ext = path.Ext(x.Name())

			// goto is silly, but its surprisingly functional here, saves writing an if check
			goto configloaderInit
		}
		return nil, ErrNoRepositoryConfig
	}

configloaderInit:

	var cro *configreader.Options = nil
	if opts.StrictParse {
		cro = &configreader.Options{
			Strict:      true,
			IndentDepth: 4,
		}
	}

	cr, _, err := temconfig.New(ext, cro)
	if err != nil {
		return nil, err
	}

	var repometa temwiki.RootMeta

	{
		f, err := wt.Filesystem.Open(ConfigRepository + cr.FileExtension())
		if err != nil {
			return nil, err
		}
		d, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, err
		}
		err = cr.Unmarshal(d, &repometa)
		if err != nil {
			return nil, err
		}
	}

	cmn := &accessorCommon{
		repo: repo,

		rmet: repometa,

		root: wt.Filesystem,

		Conf: cr,
	}

	if opts == nil {
		opts = &Options{}
	}

	return &Filesystem{
		&Accessor{
			opts:           opts,
			accessorCommon: cmn,
			readOnly:       noWrite,
		},
		opts,
		repometa,
	}, nil
}

// Filesystem is the access point to all things related to the file tree, Write contains any methods that might change the data on disk, while Read contains everything else
type Filesystem struct {
	A *Accessor // will likely be unexported once more things are complete

	Opts *Options

	RepoSettings temwiki.RootMeta
}
