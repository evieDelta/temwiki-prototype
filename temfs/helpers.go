package temfs

import (
	"os"
	"path"
	"strings"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/sid"
	"codeberg.org/eviedelta/temwiki/temerror"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

// WriteFileAndCommit writes a file to disk and then commits it using the info in who
// if who is nil it'll just act as an alias of WriteFile
func (a *Accessor) WriteFileAndCommit(who *Commit, file string, data []byte) error {
	// this aliasing behavior is relied on, don't remove it
	if who == nil {
		return a.WriteFile(file, data)
	}

	var pstate []byte

	{
		dat, err := a.ReadFile(file)
		if err != nil && !os.IsNotExist(err) {
			return err
		}
		if err != nil && os.IsNotExist(err) {
			pstate = nil
		} else {
			pstate = dat
		}
	}

	reset := func() error {
		if pstate != nil {
			err := a.WriteFile(file, pstate)
			if err != nil {
				return err
			}
		} else {
			err := a.DeleteFile(file)
			if err != nil {
				return err
			}
		}

		a.Lock()
		defer a.Unlock()

		wt, err := a.repo.Worktree()
		if err != nil {
			return err
		}

		err = wt.Reset(&git.ResetOptions{})
		if err != nil {
			return err
		}

		return nil
	}

	err := a.WriteFile(file, data)
	if err != nil {
		err2 := reset()
		if err2 != nil {
			return temerror.Newf(err, "Something really went wrong, %v", err2)
		}
		return err
	}

	err = a.CommitFile(who, file)
	if err != nil {
		err2 := reset()
		if err2 != nil {
			return temerror.Newf(err, "Something really went wrong, %v", err2)
		}
		return err
	}

	return nil
}

// ReadDirectorySubdirs lists all the subdirectories in a directory
func (a *Accessor) ReadDirectorySubdirs(dir string) (dirs []string, err error) {
	fi, err := a.ReadDirectoryContents(dir)
	if err != nil {
		return nil, err
	}

	dirs = make([]string, 0, (len(fi)))

	for _, x := range fi {
		if x.IsDir() {
			dirs = append(dirs, x.Name())
		}
	}

	return dirs, nil
}

// ReadDirectoryConfigContents returns a list of all files with the config extension in a directory as well as all subdirectories
// it trims off the config extension
func (a *Accessor) ReadDirectoryConfigContents(dir string) (confs []string, dirs []string, err error) {
	fi, err := a.ReadDirectoryContents(dir)
	if err != nil {
		return nil, nil, err
	}

	list := make([]string, 0, len(fi))
	dirs = make([]string, 0, (len(fi)))

	for _, x := range fi {
		if x.IsDir() {
			dirs = append(dirs, x.Name())
			continue
		}
		if !strings.HasSuffix(x.Name(), a.Conf.FileExtension()) {
			continue
		}
		list = append(list, strings.TrimSuffix(x.Name(), a.Conf.FileExtension()))
	}

	return list, dirs, nil
}

// ReadCategoryConfigContents returns a list of all articles (files with the config extension) in a category directory, along with all subfolders (categories)
// It trims off the config extension and drops the first directory from the output (as that usually is a folder like articles)
// and skips files starting with ! as those usually have a special purpose
func (a *Accessor) ReadCategoryConfigContents(dir string) (confs []temwiki.ArticlePath, directories []temwiki.ArticlePath, err error) {
	cs, ds, err := a.ReadDirectoryConfigContents(dir)
	if err != nil {
		return nil, nil, err
	}
	path := temwiki.ParseArticlePath(dir)

	list := make([]temwiki.ArticlePath, 0, len(cs))
	dirs := make([]temwiki.ArticlePath, 0, len(cs))

	for _, x := range cs {
		if strings.HasPrefix(x, ConfigBuiltinPrefix) {
			continue
		}
		conf := append(make(temwiki.ArticlePath, 0, len(path)+1), path[1:]...)
		conf = append(conf, x)
		list = append(list, conf)
	}
	for _, x := range ds {
		d := append(make(temwiki.ArticlePath, 0, len(path)+1), path[1:]...)
		d = append(d, x)
		dirs = append(dirs, d)
	}

	return list, dirs, nil
}

// ReadConfig reads a config formatted file from the filesystem
func (a *Accessor) ReadConfig(file string, val interface{}) error {
	if !strings.HasSuffix(file, a.Conf.FileExtension()) {
		file += a.Conf.FileExtension()
	}

	dat, err := a.ReadFile(file)
	if err != nil {
		return err
	}

	return a.Conf.Unmarshal(dat, val)
}

// WriteConfig writes to a structured config type file in the state (either a transaction or a direct accessor)
// if who is nil it'll use WriteFile, if its not nil it'll use WriteFileAndCommit
func (a *Accessor) WriteConfig(who *Commit, file string, val interface{}) error {
	if !strings.HasSuffix(file, a.Conf.FileExtension()) {
		file += a.Conf.FileExtension()
	}

	data, err := a.Conf.Marshal(val)
	if err != nil {
		return err
	}

	// the if who is nil check is located in the below function
	return a.WriteFileAndCommit(who, file, data)
}

// ChangedFiles gives you the files that have been changed since x point, you can either give the hash as a string or as a plumbing.Hash, it doesn't matter which
func (a *Accessor) ChangedFiles(shash *string, phash *plumbing.Hash) (new, modified, deleted []string, description string, err error) {
	var hash plumbing.Hash
	if shash != nil {
		h, err := a.repo.ResolveRevision(plumbing.Revision(*shash))
		if err != nil {
			return nil, nil, nil, "", err
		}
		if h == nil {
			return nil, nil, nil, "", temerror.New(nil, "something happened idk what")
		}
		hash = *h
	} else if phash != nil {
		hash = *phash
	} else {
		return nil, nil, nil, "", temerror.New(nil, "no hash given")
	}

	c, err := a.repo.CommitObject(hash)
	if err != nil {
		return nil, nil, nil, "", err
	}

	hr, err := a.repo.Head()
	if err != nil {
		return nil, nil, nil, "", err
	}
	hc, err := a.repo.CommitObject(hr.Hash())
	if err != nil {
		return nil, nil, nil, "", err
	}

	patch, err := hc.Patch(c)
	if err != nil {
		return nil, nil, nil, "", err
	}

	var nmap = make(map[string]bool)
	var mmap = make(map[string]bool)
	var dmap = make(map[string]bool)

	for _, x := range patch.FilePatches() {
		f, t := x.Files()
		switch {
		case t == nil && f != nil:
			dmap[f.Path()] = true
		case t != nil && f != nil:
			mmap[t.Path()] = true
		case t != nil && f == nil:
			nmap[t.Path()] = true
		}
	}

	new = make([]string, 0, len(nmap))
	modified = make([]string, 0, len(mmap))
	deleted = make([]string, 0, len(dmap))

	for x := range nmap {
		new = append(new, x)
	}
	for x := range mmap {
		modified = append(modified, x)
	}
	for x := range dmap {
		deleted = append(deleted, x)
	}

	// yes i am returning the variables backwards, i have no idea why but for some reason above makes them backwards
	// and doing this is an easier way to correct it than to spend ages debugging above
	return deleted, modified, new, patch.String(), nil
}

// BUG(evie) Changed since internally has the variables backwards, i do not know why the contents of the in function variables are swapped but they are, this shouldn't effect the api though as i swapped the return values

// DataModifiedFiles is the return type for Filesystem.ModifiedFiles
type DataModifiedFiles struct {
	Tags struct {
		Created, Changed []*temwiki.Tag
		Deleted          []sid.ID
	}
	Articles struct {
		Created, Changed []*temwiki.Article
		Deleted          []temwiki.ArticlePath
	}
	CateConf struct {
		Created, Changed []*temwiki.Category
		Deleted          []temwiki.ArticlePath
	}
}

// ModifiedFiles is a helper function for updating the database, it fetches all tags or articles that have been created, changed, or removed since the given commit
func (f *Filesystem) ModifiedFiles(since string) (r DataModifiedFiles, description string, err error,
) {
	created, changed, deleted, desc, err := f.A.ChangedFiles(&since, nil)
	if err != nil {
		return r, "", err
	}

	r.Tags.Created = make([]*temwiki.Tag, 0, len(created))
	r.Tags.Changed = make([]*temwiki.Tag, 0, len(changed))
	r.Tags.Deleted = make([]sid.ID, 0, len(deleted))
	r.Articles.Created = make([]*temwiki.Article, 0, len(created))
	r.Articles.Changed = make([]*temwiki.Article, 0, len(changed))
	r.Articles.Deleted = make([]temwiki.ArticlePath, 0, len(deleted))
	r.CateConf.Created = make([]*temwiki.Category, 0, len(created))
	r.CateConf.Changed = make([]*temwiki.Category, 0, len(changed))
	r.CateConf.Deleted = make([]temwiki.ArticlePath, 0, len(deleted))

	for _, x := range created {
		if strings.HasPrefix(x, "articles/!category") {
			c, err := f.GetCategoryConfig(temwiki.ParseArticlePath(path.Dir(x)))
			if err != nil {
				return r, desc, err
			}
			r.CateConf.Created = append(r.CateConf.Created, c)
		} else if strings.HasPrefix(x, "articles/") {
			a, err := f.GetArticle(temwiki.ParseArticlePath(strings.TrimPrefix(x, "articles/")))
			if err != nil {
				return r, desc, err
			}
			r.Articles.Created = append(r.Articles.Created, a)
		} else if strings.HasSuffix(x, "tags/") {
			t, err := f.GetTagString(strings.TrimPrefix(x, "tags/"))
			if err != nil {
				return r, desc, err
			}
			r.Tags.Created = append(r.Tags.Created, t)
		}
	}
	for _, x := range changed {
		if strings.HasPrefix(x, "articles/!category") {
			c, err := f.GetCategoryConfig(temwiki.ParseArticlePath(path.Dir(x)))
			if err != nil {
				return r, desc, err
			}
			r.CateConf.Changed = append(r.CateConf.Changed, c)
		} else if strings.HasPrefix(x, "articles/") {
			a, err := f.GetArticle(temwiki.ParseArticlePath(strings.TrimPrefix(x, "articles/")))
			if err != nil {
				return r, desc, err
			}
			r.Articles.Changed = append(r.Articles.Changed, a)
		} else if strings.HasSuffix(x, "tags/") {
			t, err := f.GetTagString(strings.TrimPrefix(x, "tags/"))
			if err != nil {
				return r, desc, err
			}
			r.Tags.Changed = append(r.Tags.Changed, t)
		}
	}
	for _, x := range deleted {
		if strings.HasPrefix(x, "articles/!category") {
			apt := temwiki.ParseArticlePath(strings.TrimPrefix(x, "articles/"))
			r.CateConf.Deleted = append(r.CateConf.Deleted, apt)
		} else if strings.HasPrefix(x, "articles/") {
			apt := temwiki.ParseArticlePath(strings.TrimPrefix(x, "articles/"))
			r.Articles.Deleted = append(r.Articles.Deleted, apt)
		} else if strings.HasPrefix(x, "tags/") {
			tid, err := sid.ParseIDString(strings.TrimPrefix(x, "tags/"))
			if err != nil {
				return r, desc, err
			}
			r.Tags.Deleted = append(r.Tags.Deleted, tid)
		}
	}

	return r, desc, nil
}

// LatestCommit returns the hash of the newest commit
func (f *Filesystem) LatestCommit() (string, error) {
	h, err := f.A.repo.Head()
	if err != nil {
		return "", err
	}
	return h.Hash().String(), nil
}
