package temfs

import (
	"os"
	"time"

	"codeberg.org/eviedelta/temwiki"
	"codeberg.org/eviedelta/temwiki/internal/dtrace"
	"codeberg.org/eviedelta/temwiki/temfs/temconfig"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
)

func Initialise(where, format string) (repo *git.Repository, err error) {
	cr, _, err := temconfig.New(format, nil)
	if err != nil {
		return nil, err
	}

	repo, err = git.PlainInit(where, false)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			os.RemoveAll(where)
		}
	}()

	wt, err := repo.Worktree()
	if err != nil {
		return nil, err
	}

	dirs := []string{
		DirectoryTags,
		DirectoryArticles,
		DirectoryConfigs,
	}

	for _, x := range dirs {
		err = wt.Filesystem.MkdirAll(x, 664)
		if err != nil {
			return nil, err
		}
		_, err := wt.Add(x)
		if err != nil {
			return nil, err
		}
	}

	rcon, err := wt.Filesystem.OpenFile(ConfigRepository+cr.FileExtension(), os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		return nil, err
	}
	defer rcon.Close()
	rcdt, err := cr.Marshal(&temwiki.RootMeta{
		Version: temwiki.FormatVersion,
	})
	if err != nil {
		return nil, err
	}
	_, err = rcon.Write(rcdt)
	if err != nil {
		return nil, err
	}
	h, err := wt.Add(ConfigRepository + cr.FileExtension())
	if err != nil {
		return nil, err
	}

	dtrace.Println("added ", h)

	hash, err := wt.Commit("initialisation", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "~system~",
			Email: "",
			When:  time.Now(),
		},
	})
	if err != nil {
		return nil, err
	}

	dtrace.Println(hash)

	return
}
