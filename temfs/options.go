package temfs

import (
	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"golang.org/x/crypto/openpgp"
)

// Commit is used to mark an author and msg meta of a save/write action for git commits,
// in most cases if set to nil it'll then just not commit
// which can be used for the local cmd tools to allow one to manually add the files to the commit via standard git tooling
type Commit struct {
	Name    string // Username
	EmailID string // email or user ID
	Why     string // Commit message

	UseConfig bool // this is used primarily for local tools
	//             // it'll tell it to not use the info here
	//             // and instead read the author info from the standard git config
}

// Options contains some etc options
type Options struct {
	// StrictParse is used for vet checking and enables stricter parsing (no unknown keys and no duplicate keys)
	StrictParse bool

	// a PGP key used for signing and verifying commits made internally by the officially hosted api or bot instance to prevent spoofing commits by external users
	PGPKey *openpgp.Entity

	// ValidKeys is a list of pgp keys that are recognised as valid and can be used to mark an edit as verified
	ValidKeys *crypto.KeyRing

	// yes i am importing 2 openpgp libraries here, yes i know its inefficient
	// i just can't be bothered to try figure out one, but its required for go-git
	// and the other is easier for me to figure out but its using a forked x/crypto so it doesn't work directly with go-git
}
