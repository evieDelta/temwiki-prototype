package configreader

// Options defines some options configreaders may use, be aware that not all of these may be supported for all implementations
type Options struct {
	Strict bool

	IndentDepth  int
	IndentString string
}

// Creator is used for creating new config readers of a format, a configreader may return if any options are unsupported via the middle string return value,
// a config reader must be able to handle the default 0 settings as that is what will be passed if a user doesn't specify any
type Creator interface {
	New(opts *Options) (cr Interface, unsupported string, err error)
}

// Interface allows inserting a custom config format for storing data on disk as an alternative to the default yaml format.
// Be aware that once you pick one you'll want to stick with it,
// as converting down the line may be a pain and there are no built in facilities to handle multiple formats within a repo or convert a repo from one format to another.
// a repo may specify which format it uses by
type Interface interface {
	Unmarshal([]byte, interface{}) error
	Marshal(interface{}) ([]byte, error)

	FileExtension() string
}
