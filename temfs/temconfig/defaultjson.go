package temconfig

import (
	"bytes"
	"encoding/json"
	"strconv"
	"strings"

	"codeberg.org/eviedelta/temwiki/temfs/temconfig/configreader"
)

const extensionJSON = ".json"

func init() {
	Register(extensionJSON, NewJSONReader{})
}

type NewJSONReader struct{}

func (n NewJSONReader) New(opt *configreader.Options) (configreader.Interface, string, error) {
	var unsup string
	if opt != nil {
		if opt.IndentDepth != 0 {
			unsup += "Option `IndentDepth` is not supported. "
			if opt.IndentString == "" {
				opt.IndentString = strings.Repeat(" ", opt.IndentDepth)
				l := strconv.Itoa(opt.IndentDepth)
				unsup += "Option `IndentDepth` converted from " + l + " into Option `IndentString` as " + l + "x Space. "
			}
		}
		if opt.IndentString == "" {
			opt.IndentString = "	" // tab
		}
	} else {
		opt = &configreader.Options{
			IndentString: "	", // tab
		}
	}
	i := &JSONReader{
		Strict:      opt.Strict,
		Indentation: opt.IndentString,
	}

	return i, unsup, nil
}

// JSONReader is the default reader
type JSONReader struct {
	Strict      bool
	Indentation string
}

func (y *JSONReader) FileExtension() string {
	return extensionJSON
}

func (y *JSONReader) Marshal(val interface{}) ([]byte, error) {
	buf := bytes.NewBuffer(make([]byte, 0, 5000)) // should be big enough for most things

	e := json.NewEncoder(buf)
	e.SetIndent("", y.Indentation)

	err := e.Encode(val)
	return buf.Bytes(), err
}

func (y *JSONReader) Unmarshal(data []byte, val interface{}) error {
	d := json.NewDecoder(bytes.NewBuffer(data))
	if y.Strict {
		d.DisallowUnknownFields()
	}
	return d.Decode(val)
}
