package temconfig

import (
	"bytes"
	"strconv"
	"strings"

	"codeberg.org/eviedelta/temwiki/temfs/temconfig/configreader"
	"github.com/pelletier/go-toml"
)

const extensionToml = ".toml"

func init() {
	Register(extensionToml, NewTomlReader{})
}

type NewTomlReader struct{}

func (n NewTomlReader) New(opt *configreader.Options) (configreader.Interface, string, error) {
	var unsup string
	if opt != nil {
		if opt.IndentDepth != 0 {
			unsup += "Option `IndentDepth` is not supported. "
			if opt.IndentString == "" {
				opt.IndentString = strings.Repeat(" ", opt.IndentDepth)
				l := strconv.Itoa(opt.IndentDepth)
				unsup += "Option `IndentDepth` converted from " + l + " into Option `IndentString` as " + l + "x Space. "
			}
		}
		if opt.IndentString == "" {
			opt.IndentString = "	" // tab
		}
	} else {
		opt = &configreader.Options{
			IndentString: "	", // tab
		}
	}
	i := &TomlReader{
		Strict:      opt.Strict,
		Indentation: opt.IndentString,
	}

	return i, unsup, nil
}

// TomlReader is the default reader
type TomlReader struct {
	Strict      bool
	Indentation string
}

func (y *TomlReader) FileExtension() string {
	return extensionToml
}

func (y *TomlReader) Marshal(val interface{}) ([]byte, error) {
	buf := bytes.NewBuffer(make([]byte, 0, 5000)) // should be big enough for most things
	e := toml.NewEncoder(buf).
		ArraysWithOneElementPerLine(true).
		Indentation(y.Indentation)

	err := e.Encode(val)

	return buf.Bytes(), err
}

func (y *TomlReader) Unmarshal(data []byte, val interface{}) error {
	d := toml.NewDecoder(bytes.NewBuffer(data))
	d.Strict(y.Strict)
	return d.Decode(val)
}
