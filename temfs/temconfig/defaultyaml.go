package temconfig

import (
	"strconv"
	"strings"

	"codeberg.org/eviedelta/temwiki/temfs/temconfig/configreader"
	"github.com/goccy/go-yaml"
)

func init() {
	y := NewYamlReader{}
	Register("", &y)
	Register(".yaml", &y)
}

type NewYamlReader struct{}

func (n NewYamlReader) New(opt *configreader.Options) (configreader.Interface, string, error) {
	var unsup string
	if opt != nil {
		if opt.IndentString != "" {
			unsup += "Option `IndentString` not supported. "

			if opt.IndentDepth == 0 {
				if opt.IndentString == "	" /* tab */ {
					opt.IndentDepth = 4 // assume tab means 4 spaces
					unsup += "Converted Option `IndentString` from 1 tab into Option `IndentDepth` as 4."
				}
				// if IndentString only contains space and nothing else set IndentDepth to it
				if strings.IndexFunc(opt.IndentString, func(r rune) bool { return r != ' ' }) == -1 {
					opt.IndentDepth = len([]rune(opt.IndentString))
					l := strconv.Itoa(opt.IndentDepth)
					unsup += "Converted Option `IndentString` from " + l + "x Space into Option `IndentDepth` as " + l
				}
			}
		}
		if opt.IndentDepth == 0 {
			opt.IndentDepth = 4
		}
	} else {
		opt = &configreader.Options{
			IndentDepth: 2,
		}
	}
	i := &YamlReader{
		EncodeSettings: []yaml.EncodeOption{
			yaml.Indent(opt.IndentDepth),
		},
		DecodeSettings: []yaml.DecodeOption{},
	}
	if opt.Strict {
		i.DecodeSettings = append(i.DecodeSettings, yaml.Strict())
	}
	return i, unsup, nil
}

// YamlReader is the default reader
type YamlReader struct {
	EncodeSettings []yaml.EncodeOption
	DecodeSettings []yaml.DecodeOption
}

func (y *YamlReader) FileExtension() string {
	return ".yaml"
}

func (y *YamlReader) Marshal(val interface{}) ([]byte, error) {
	return yaml.MarshalWithOptions(val, y.EncodeSettings...)
}

func (y *YamlReader) Unmarshal(data []byte, val interface{}) error {
	return yaml.UnmarshalWithOptions(data, val, y.DecodeSettings...)
}
