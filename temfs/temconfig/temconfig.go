package temconfig

import (
	"errors"
	"fmt"

	"codeberg.org/eviedelta/temwiki/temfs/temconfig/configreader"
)

type Loader configreader.Interface

var formats = map[string]configreader.Creator{}

// Register adds a config format to the recognised list of config formats,
// like the function of the same name in "database/sql" it panics if there is already one registered with the same name, or if c is nil
func Register(extension string, c configreader.Creator) {
	if formats[extension] != nil {
		panic(fmt.Sprint("Cannot define duplicate configreader, ", extension, " was already defined"))
	}
	if c == nil {
		panic(fmt.Sprint("Cannot add a nil configreader, ", extension, "was nil"))
	}
	formats[extension] = c
}

// New returns a new config reader of a perticular format, it will return ErrorNotSupported if extension is not defined anywhere,
// Opts contains optional settings for configreaders note that not all implementations may support all settings,
// a configreader may return if any options are unsupported via the middle string return value though in most cases it is safe to ignore but its still good to be aware of
func New(extension string, opts *configreader.Options) (cr configreader.Interface, unsupported string, err error) {
	if formats[extension] == nil {
		return nil, "", ErrorNotSupported
	}
	return formats[extension].New(opts)
}

var ErrorNotSupported = errors.New("Unsupported format...")
